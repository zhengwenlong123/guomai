// 公共引用部分，兼容IE8
import '/apps/common/common';
import '/apps/common/common-index';
import './sszhxt.css';
import 'ane';
import {
    message,
    notification
} from 'ane';
import * as menuService from '../../services/menuService';
import {
    copyRight,
    telephone,
    defalutCity,
    mapType
} from '/services/configService';
import ajax from '../../services/ajaxService';
import * as cityobj from '../../apps/common/common-sszhmap-cityaddress';
import {
    webSocketIp
} from '../../services/configService';
import io from 'socket.io-client';
let {
    routerserver
} = require('/services/routerService');
require('../../apps/common/common-poll-sidebar');
require('../../apps/common/common-sszh-sidebar');
require("../../apps/common/common-sszh-mapcity.js");

let storage = require('../../services/storageService').ret;

let userName = storage.getItem('userName');
let orgName = storage.getItem('orgName');
let roleNames = storage.getItem('roleNames');
let frameWindow;
if (userName != null && userName != '' && roleNames != null) {} else { //无登录信息时退出并跳转登录页
    storage.clearAll();
    global.location.href = '/main-login.html';
}

var root = avalon.define({
    $id: 'sszhxt_vm',
    currentPage: '',
    userName: userName,
    orgName: orgName,
    roleNames: roleNames,
    titleName: '',
    roleShow: false,
    copyRight: copyRight,
    telephone: telephone,
    locationKey: window.location.hash ? window.location.hash.slice(2) : '/sszhxt-sszh',
    menu: get_menu(),
    menuClick(index, item) {
        if (item.url) {
            this.locationKey = item.url;
        }

    },
    toggleChildrenItem(event) {
        $(event.target).find('.ant-menu-item-children') && $(event.target).find('.ant-menu-item-children').toggle();
    },
    $cityDetailobj: {
        cityobj: cityobj
    },

    // sos告警信息菜单切换
    sosMenuType: 'service',
    sosInfoShow: false, //告警信息是否展示
    serviceSOSData: [], //业务告警数据
    deviceSOSData: [], //设备告警数据
    statusSOSData: [], //状态告警数据
    sosMenuClick(val) { // sos告警信息菜单切换
        this.sosMenuType = val;
    },
    sosClick() {
        if (this.sosInfoShow) {
            this.sosInfoShow = false;
        } else {
            this.sosInfoShow = true;
        }
    },
    handelSOS(item) {
        if (item.status == 1) return;
        root.sosInfoShow = false;
        item.status = 1;
        item.mytype = 0;
        if (window.location.hash.replace('#!/', '') == 'sszhxt-sszh') {
            avalon.vmodels['sszhxt_sszh'].handleTreeCheck('', '', item, true);
            return;
        }
        storage.setItem('sszhxt-SOS', item);
        var baseUrl = document.location.href.substring(0, document.location.href.lastIndexOf("/"));
        window.location.href = baseUrl + "/sszhxt-sszh";
    },
    showMessage: showMessage
});

// 动态设置title名字
menuService.sysMenu.then(menu => {
    let sysList = menu.sysList;
    avalon.each(sysList, (key, val) => {
        if(/^\/sszhxt.html/.test(val.indexUrl)) {
            document.title = val.title;
            root.titleName = val.title;
        }
    });
});

if (root.roleNames.length == 0) {
    roleNames.push(' - ');
}
if (root.roleNames.length > 1) {
    root.roleShow = true;
}
//提示框提示
function showMessage(type, content) {
    notification[type]({
        title: "通知",
        message: content
    });
}

function get_menu() {
    let list = [{
            key: "sszhxt-sszh",
            title: "实时指挥",
            url: '/sszhxt-sszh',
            lic: 'INSTRUCT_MENU_SSZH',
            active: false
        },
        {
            key: "sszhxt-spjk",
            title: "视频监控",
            url: '/sszhxt-spjk',
            lic: 'INSTRUCT_MENU_SPJK',
            active: false
        },
        {
            key: "sszhxt-lxhf",
            title: "录像回放",
            url: '/sszhxt-lxhf',
            lic: 'INSTRUCT_MENU_LXHF',
            active: false
        },
        {
            key: "sszhxt-gjcx",
            title: "轨迹查询",
            url: '/sszhxt-gjcx',
            lic: 'INSTRUCT_MENU_GJCX',
            active: false
        },
        {
            key: "sszhxt-gjgl",
            title: "告警管理",
            lic: 'INSTRUCT_MENU_GJGL',
            active: false,
            children: [{
                key: 'gjcx',
                title: '告警查询',
                lic: 'INSTRUCT_MENU_GJGL_GJCX',
                url: '/sszhxt-gjgl-gjcx',
                active: false
            }, {
                key: 'gjdy',
                title: '告警订阅',
                lic: 'INSTRUCT_MENU_GJGL_GJDY',
                url: '/sszhxt-gjgl-gjdy',
                active: false
            }, {
                key: 'gjsz',
                title: '告警设置',
                lic: 'INSTRUCT_MENU_GJGL_GJSZ',
                url: '/sszhxt-gjgl-gjsz',
                active: false
            }]
        },
        {
            key: "sszhxt-dzwl",
            title: "电子围栏",
            url: '/sszhxt-dzwl',
            lic: 'INSTRUCT_MENU_DZWL',
            active: false
        },
        {
            key: "sszhxt-jqdj",
            title: "集群对讲",
            url: '/sszhxt-jqdj',
            lic: 'INSTRUCT_MENU_JQDJ',
            active: false
        },
        {
            key: "sszhxt-xxcj",
            title: "信息采集",
            url: '/sszhxt-xxcj',
            lic: 'SSZH_MENU_XXCJ',
            active: false
        }
    ];

    menuService.menu.then(menu => {
        let remote_list = menu.SSZH_MENU_SSZHXT; //实时指挥系统平台已授权的所有菜单及功能权限数组
        let get_list = []; //过滤出来的一级菜单数组
        let output_list = []; //已授权的菜单map数组

        avalon.each(remote_list, function (index, el) {
            if (/^INSTRUCT_MENU_/.test(el))
                avalon.Array.ensure(get_list, el);
        });

        avalon.each(list, function (index, el) {
            avalon.each(get_list, function (idx, ele) {
                let child_list = [];
                if (!el.hasOwnProperty("children") || 0 == el.children.length) {

                } else {
                    avalon.each(el.children, function (k, v) {
                        avalon.each(get_list, function (kk, vv) {
                            if (vv == v.lic) {
                                // avalon.Array.ensure(child_list, v);
                                if (menu.SSZH_MENU_SSZHXT_ARR[v.lic])
                                    v.title = menu.SSZH_MENU_SSZHXT_ARR[v.lic];
                                child_list.push(v);
                                return;
                            }
                        });
                        el.children = child_list;
                    });
                }

                if (ele == el.lic) {
                    if (menu.SSZH_MENU_SSZHXT_ARR[el.lic])
                        el.title = menu.SSZH_MENU_SSZHXT_ARR[el.lic];
                    avalon.Array.ensure(output_list, el);
                }
            });
        });
        root.menu = output_list;
        // 默认选中第一项菜单（注：shortcutList.js里面的实时指挥系统写死了路由，故可知第一次点击的hash是/sszhxt-sszh）
        if (root.locationKey == '/sszhxt-sszh') {
            avalon.history.setHash(root.menu[0].url);
        }
    });
}
//router server
routerserver('sszhxt_vm');

avalon.bind(window, 'hashchange', (e) => {
    root.locationKey = window.location.hash ? window.location.hash.slice(2) : '/sszhxt-sszh';
});

// history
avalon.history.start({
    root: "/",
    fireAnchor: false
});

avalon.scan(document.body);

//后台获取当前用户设置的地图默认城市
ajax({
    url: '/gmvcs/uap/user/city/get',
    method: 'get',
    data: null,
    async: false
}).then(result => {
    if (result.code == 0) {
        let city;
        if (result.data != null || result.data != undefined) {
            city = result.data.city;
        } else {
            city = defalutCity;

        }
        root.$cityDetailobj.defaultcity = city;
        root.$cityDetailobj.nowClickcity = city;
        root.$cityDetailobj.lon = cityobj[city].lon;
        root.$cityDetailobj.lat = cityobj[city].lat;
        // avalon.vmodels['sszhmap'].showcityName = city;
        // avalon.vmodels['citycontroller'].nowcity = city;
        // avalon.vmodels['citycontroller'].defaultcity = city;
        // if($('#mapIframe')[0].contentWindow.esriMap){
        //     $('#mapIframe')[0].contentWindow.esriMap.setMapCenter(root.$cityDetailobj.lon,root.$cityDetailobj.lat,13);
        // }
    }
})
avalon.ready(function () {
    if (mapType == 0) {
        $('#mapIframe').attr('src', '../sszhEsriMap.html');
    } else if (mapType === 1) {
        $('#mapIframe').attr('src', '../baiduMap.html');
    } else {
        $('#mapIframe').attr('src', '../mapLite.html');

    }
})
//系统第一次加载地图时加上蒙版(注意要在avalon.scan之后，否则需要定时器延时)
frameWindow = document.getElementById("mapIframe").contentWindow;
let isIE = isIE();
if ($('.map-iframe-wrap').is(':visible') && $('.map-iframe-wrap').width() > 0) {
    let iframe = isIE ? '' : '<div id="back-iframe" style="display: none;position:absolute;width:100%;height:100%;top:0;left:0;"><iframe src="about:blank" marginheight="0" marginwidth="0" style="position:absolute; visibility:inherit;top: 30%; left: 50%; width: 276px;height:36px;margin-left:-138px;z-index:992;opacity:1;filter:alpha(opacity=0);background: #4d4d4d;" frameborder="0"></iframe></div>';
    let $backdrop = $(iframe + '<div class="backdrop-loading"><span class="fa fa-spinner fa-pulse"></span>正在加载地图，请稍后<iframe src="about:blank" frameBorder="0" marginHeight="0" marginWidth="0" style="position:absolute; visibility:inherit; top:0px;left:0px;width:100%; height:100%;z-index:-1;opacity:0;filter:alpha(opacity=0);"></iframe></div><div class="backdrop"><iframe src="about:blank" frameBorder="0" marginHeight="0" marginWidth="0" style="position:absolute; visibility:inherit; top:0px;left:0px;width:100%; height:100%;z-index:-1;opacity:0;filter:alpha(opacity=0);"></iframe></div>');
    $('body').append($backdrop);
    $('#back-iframe').show();
}
if (mapType == 0) {
    frameWindow.defineTimer = setInterval(() => {
        frameWindow = document.getElementById("mapIframe").contentWindow;
        if (frameWindow.loadMapCompelete) {
            clearInterval(frameWindow.defineTimer);
            $('#back-iframe,.backdrop-loading,.backdrop').remove();
        }
    }, 200);
}

function isIE() {
    if (!!window.ActiveXObject || "ActiveXObject" in window)
        return true;
    else
        return false;
}



// $(function(){
//     $("sos-info-menu > li").click(function(){
//         $("sos-info-menu > li").removeClass("active");
//         $(this).addClass("active");
//         // var src=event.target;//get a
//         // var href=$(src).attr("target");//get t
//         // var dd="#"+href; //get id
//         console.log(this);

//     });
// });

//=====================================sos告警===========================
var error_total = 0;
const websocketIP = 'ws://' + webSocketIp;
const socket = io(websocketIP, {
    transports: ['polling']
});

socket.on('connect', (connect) => {
    //获取当前登录用户的部门path
    ajax({
        url: '/gmvcs/uap/cas/loginUser',
        method: 'get',
        data: null
    }).then(result => {
        if (result.code != 0) {
            return;
        }
        let uid = result.data.uid;
        socket.emit('uidEvent', {
            'uid': uid
        });
    });
});

socket.on('messageevent', (ret) => {
    ret.status = 0;
    if (ret.sosType == "DEVICE_SOS") {
        if (ret.sosSource == "DEVICE_ELECTRIC_CAPACITANCE") {
            ret.sosType = '设备电量';
        } else {
            ret.sosType = '设备容量';
        }

        if (root.deviceSOSData.length > 500) {
            root.deviceSOSData.clear();
        }
        root.deviceSOSData.unshift(ret);
    } else if (ret.sosType == "BUSINESS_SOS") {
        ret.gbcode = ret.deviceId;
        root.sosInfoShow = true;
        root.sosMenuType = 'service';
        ret.sosType = 'SOS告警';
        root.serviceSOSData.unshift(ret);
    } else {
        if (ret.sosSource == "DEVICE_ONLINE") {
            ret.sosType = '设备上线';
        } else {
            ret.sosType = '设备下线';
        }
        if (root.statusSOSData.length > 500) {
            root.statusSOSData.clear();
        }
        root.statusSOSData.unshift(ret);
    }

});

socket.on('STATUS', (ret) => {
    // if(ret.closed == "true") {
    //     socket.close();
    // }
});

socket.on('error', (error) => {
    socket.close();
});
socket.on('reconnecting', (error) => {
    //console.log(error);
});
socket.on('connect_error', (error) => {
    error_total++;
    if (error_total > 9) {
        error_total = 0;
        socket.close();
        // console.log("=====重连次数超出限制，已断开socket连接=====");
    }
});

socket.on('connect_timeout', (timeout) => {
    // console.log("connect_timeout:", timeout);
});