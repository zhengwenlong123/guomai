// 公共引用部分，兼容IE8
import '/apps/common/common';
import './zfsypsjglpt.css';
import '/apps/common/common-index';
import 'ane';
import {
    applyRouteConfig
} from '/services/pagesRouterServer';
import * as menuService from '../../services/menuService';
import {
    store
} from '/apps/common/common-store.js';
import {
    copyRight,
    telephone,
    playerConfig
} from '/services/configService';

import {
    list as listMenu,
    zfdaMenu,
    jdkpMenu
} from './menuConfig';
import {
    routeConfig
} from './routeConfig';

let delete_ocx = require('../../apps/common/common').delete_ocx;
let storage = require('../../services/storageService').ret;
let userName = storage.getItem('userName');
let orgName = storage.getItem('orgName');
let roleNames = storage.getItem('roleNames');
if (userName != null && userName != '' && roleNames != null) {} else { //无登录信息时退出并跳转登录页
    storage.clearAll();
    global.location.href = '/main-login.html';
}

var root = avalon.define({
    $id: 'zfsypsjglpt_vm',
    currentPage: '',
    copyRight: copyRight,
    telephone: telephone,
    userName: userName,
    orgName: orgName,
    roleNames: roleNames,
    titleName: '执法视音频数据管理系统',
    roleShow: false,
    year: (new Date()).getFullYear(),
    menu: get_menu(),
    selectedKeys: [],
    openKeys: ['yspk'],
    flag_jtwfkp: false,
    handleMenuClick(item, key, keyPath) {
        let userAgent = navigator.userAgent;
        if (userAgent.indexOf("Chrome") > -1 && userAgent.indexOf("Safari") > -1)
            delete_ocx();

        // avalon.router.navigate(item.url, 2);
        avalon.history.setHash(item.url);

        if (item.url == '/zfsypsjglpt-jdkp-jycx_jiaojing') {
            this.flag_jtwfkp = true;
            root.selectedKeys = new Array('zfsypsjglpt-jdkp-jycx_jiaojing');
        } else {
            this.flag_jtwfkp = false;
        }
    },
    open_arr: ['yspk'],
    handleOpenChange(open_arr) {
        this.open_arr = open_arr.splice(-1, 1);
        this.openKeys = this.open_arr;
    },
    show_player: playerConfig
});
if (root.roleNames.length == 0) {
    roleNames.push(' - ');
}
if (root.roleNames.length > 1) {
    root.roleShow = true;
}

// 动态设置title名字
menuService.sysMenu.then(menu => {
    let sysList = menu.sysList;
    avalon.each(sysList, (key, val) => {
        if (/^\/zfsypsjglpt.html/.test(val.indexUrl)) {
            document.title = val.title;
            root.titleName = val.title;
        }
    });
});

let output_list = []; //已授权的菜单map数组
let authorized3LevelList = []; // 已授权的第三级菜单
function get_menu() {

    let list = listMenu;
    // 第三级菜单 执法档案
    let level3List = zfdaMenu;

    menuService.menu.then(menu => {
        let remote_list = menu.AUDIO_MENU_SYPSJGL; //音视频数据管理平台已授权的所有菜单及功能权限数组
        let get_list = []; //过滤出来的一级菜单数组

        // remote_list.push('AUDIO_MENU_TJFX_GLQKTJ'); //关联情况统计（公安版本）全警种测试

        avalon.each(remote_list, function (index, el) {
            if (/^AUDIO_MENU_/.test(el))
                avalon.Array.ensure(get_list, el);
        });

        let filterMenu = (list, output) => {
            avalon.each(list, function (index, el) {
                avalon.each(get_list, function (idx, ele) {
                    if (ele == el.lic) {
                        let child_list = [];
                        if (!el.hasOwnProperty("children") || 0 == el.children.length) {

                        } else {
                            avalon.each(el.children, function (k, v) {
                                avalon.each(get_list, function (kk, vv) {
                                    if (vv == v.lic) {
                                        // avalon.Array.ensure(child_list, v);
                                        if (menu.AUDIO_MENU_SYPSJGL_ARR[v.lic])
                                            v.title = menu.AUDIO_MENU_SYPSJGL_ARR[v.lic];
                                        child_list.push(v);
                                        return;
                                    }
                                });
                                el.children = child_list;
                            });
                        }

                        if (menu.AUDIO_MENU_SYPSJGL_ARR[el.lic])
                            el.title = menu.AUDIO_MENU_SYPSJGL_ARR[el.lic];
                        avalon.Array.ensure(output, el);
                        return;
                    }
                });
            });
        };

        filterMenu(list, output_list);
        filterMenu(level3List, authorized3LevelList);

        store.dispatch({
            type: "saveMenu",
            menu: authorized3LevelList
        });

        if (!/zfsypsjglpt/.test(global.location.hash)) { //默认选择第一个模块
            if (output_list[0].children.length > 0) {
                avalon.history.setHash(output_list[0].children[0].url);
                let current_key = window.location.hash.split("/")[1];
                root.openKeys = new Array(current_key.split("-")[1]);
                root.open_arr = new Array(current_key.split("-")[1]);
                root.selectedKeys = new Array(output_list[0].children[0].key);

            } else {
                avalon.history.setHash(output_list[0].url);
                root.openKeys = new Array(output_list[0].key);
                root.open_arr = new Array(output_list[0].key);
                root.selectedKeys = new Array(output_list[0].key);

            }
        }

        root.menu = output_list;
    });
}
//===============================================================================================================
let page_controller = avalon.define({
    $id: "jtwfkp_page_controller",
    selectedKeys: [],
    currentPage: '',
    openKeys: ['yspk'],
    handleMenuClick(item, key, keyPath) {
        // avalon.router.navigate(item.url, 2);
        avalon.history.setHash(item.url);
        return;
    },
    open_arr: ['yspk'],
    handleOpenChange(open_arr) {
        this.open_arr = open_arr;
    },
    menu: get_menu_jtwfkp()
});
let output_list_jtwfkp = []; //已授权的菜单map数组 
function get_menu_jtwfkp() {

    // 监督考评三级菜单
    let list = jdkpMenu;

    menuService.menu.then(menu => {
        let remote_list = menu.AUDIO_MENU_SYPSJGL; //音视频数据管理平台已授权的所有菜单及功能权限数组

        let get_list = []; //过滤出来的一级菜单数组

        avalon.each(remote_list, function (index, el) {
            if (/^AUDIO_FUNCTION_/.test(el))
                avalon.Array.ensure(get_list, el);
        });

        avalon.each(list, function (index, el) {
            avalon.each(get_list, function (idx, ele) {
                if (ele == el.lic) {
                    let child_list = [];
                    if (!el.hasOwnProperty("children") || 0 == el.children.length) {

                    } else {
                        avalon.each(el.children, function (k, v) {

                            avalon.each(get_list, function (kk, vv) {
                                if (vv == v.lic) {
                                    // avalon.Array.ensure(child_list, v);
                                    child_list.push(v);
                                    return;
                                }
                            });

                            el.children = child_list;
                        });
                    }

                    avalon.Array.ensure(output_list_jtwfkp, el);
                    return;
                }
            });
        });

        if (!/zfsypsjglpt/.test(global.location.hash)) { //默认选择第一个模块
            if (output_list_jtwfkp[0].children.length > 0) {
                avalon.history.setHash(output_list_jtwfkp[0].children[0].url);
                let current_key = window.location.hash.split("/")[1];
                root.openKeys = new Array(current_key.split("-")[1]);
                root.open_arr = new Array(current_key.split("-")[1]);
                root.selectedKeys = new Array(output_list_jtwfkp[0].children[0].key);

            } else {
                avalon.history.setHash(output_list_jtwfkp[0].url);
                root.openKeys = new Array(output_list_jtwfkp[0].key);
                root.open_arr = new Array(output_list_jtwfkp[0].key);
                root.selectedKeys = new Array(output_list_jtwfkp[0].key);

            }
        }

        page_controller.menu = output_list_jtwfkp;
    });
}

$(document).ready(function () {
    get_selectedKey(true);
});

applyRouteConfig(routeConfig, {
    name: 'zfsypsjglpt_vm'
});

avalon.history.start({
    root: "/",
    fireAnchor: false
});

if (!/#!/.test(global.location.hash)) {
    avalon.router.navigate('/', 2);
}

avalon.bind(window, "hashchange", (e) => {
    $("#iframe_zfsyps").css({
        width: "0px",
        height: "0px",
        left: "0px",
        top: "0px"
    });
    get_selectedKey(false);
});

avalon.scan(document.body);

function get_selectedKey(val) {
    let select_key = window.location.hash.split("/")[1];
    if (!select_key)
        return;
    if (select_key.split("-").length > 3) {
        let str_index = find_str(select_key, "-", 2);
        let selectedKey_txt = select_key.slice(0, str_index);

        if (select_key.indexOf("_gongan") > -1)
            selectedKey_txt += "_gongan";
        else if (select_key.indexOf("_jiaojing") > -1)
            selectedKey_txt += "_jiaojing";

        root.selectedKeys = new Array(selectedKey_txt);
    } else
        root.selectedKeys = new Array(select_key);

    if (val) {
        root.openKeys = new Array(select_key.split("-")[1]);
        root.open_arr = new Array(select_key.split("-")[1]);
    }
}

function find_str(str, cha, num) { //找到某字符在字符串中出现第N次的位置。str 字符串，cha 某字符，num 第N次
    var x = str.indexOf(cha);
    for (var i = 0; i < num; i++) {
        x = str.indexOf(cha, x + 1);
    }
    return x;
}