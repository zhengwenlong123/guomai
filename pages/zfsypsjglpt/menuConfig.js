//菜单配置文件

// 执法视音频
export let list = [{
        key: 'zfsypsjglpt-xtsy-index',
        title: '系统首页',
        icon: 'xtsy',
        lic: 'AUDIO_MENU_XTSY', // lic 授权校验字段
        children: [],
        url: '/zfsypsjglpt-xtsy-index'
    },
    {
        key: 'sypgl',
        title: '视音频管理',
        icon: 'yspk',
        lic: 'AUDIO_MENU_SYPGL', // lic 授权校验字段
        children: [{
            key: 'zfsypsjglpt-sypgl-zfjlysyp',
            title: '执法记录仪视音频',
            lic: 'AUDIO_MENU_SYPGL_ZFYSYP_JJ',
            url: '/zfsypsjglpt-sypgl-zfjlysyp'
        }]
    },
    {
        key: 'zfda',
        title: '执法档案',
        icon: 'zfda',
        lic: 'AUDIO_MENU_ZFDA',
        children: [{
                key: 'zfsypsjglpt-zfda-jqgl_gongan',
                title: '警情管理',
                lic: 'AUDIO_MENU_ZFDA_JQGL',
                url: '/zfsypsjglpt-zfda-jqgl_gongan'
            },
            {
                key: 'zfsypsjglpt-zfda-ajgl_gongan',
                title: '案件管理',
                lic: 'AUDIO_MENU_ZFDA_AJGL',
                url: '/zfsypsjglpt-zfda-ajgl_gongan'
            },
            {
                key: 'zfsypsjglpt-zfda-jycx_jiaojing',
                title: '简易程序',
                lic: 'AUDIO_MENU_ZFDA_JTWF_JYCX',
                children: [],
                url: '/zfsypsjglpt-zfda-jycx_jiaojing'
            },
            {
                key: 'zfsypsjglpt-zfda-fxccf_jiaojing',
                title: '非现场处罚',
                lic: 'AUDIO_MENU_ZFDA_JTWF_FXCCL',
                children: [],
                url: '/zfsypsjglpt-zfda-fxccf_jiaojing'
            },
            {
                key: 'zfsypsjglpt-zfda-qzcs_jiaojing',
                title: '强制措施',
                lic: 'AUDIO_MENU_ZFDA_JTWF_QZCS',
                children: [],
                url: '/zfsypsjglpt-zfda-qzcs_jiaojing'
            },
            {
                key: 'zfsypsjglpt-zfda-sgcl_jiaojing',
                title: '事故处理',
                lic: 'AUDIO_MENU_ZFDA_SGCL',
                url: '/zfsypsjglpt-zfda-sgcl_jiaojing'
            }
            // {
            //     key: 'zfsypsjglpt-zfda-jtwf',
            //     title: '交通违法',
            //     lic: 'AUDIO_MENU_ZFDA_JTWF',
            //     url: '/zfsypsjglpt-zfda-jtwf'
            // }
        ]
    },
    {
        key: 'jdkp',
        title: '监督考评',
        icon: 'jdkp',
        lic: 'AUDIO_MENU_JDKP',
        children: [{
                key: 'zfsypsjglpt-jdkp-kphczl_gongan',
                title: '考评抽查总览-公安',
                lic: 'AUDIO_MENU_JDKP_KPCCZL_GA',
                url: '/zfsypsjglpt-jdkp-kphczl_gongan'
            },
            {
                key: 'zfsypsjglpt-jdkp-jqkp_gongan',
                title: '警情考评',
                lic: 'AUDIO_MENU_JDKP_JQKP',
                url: '/zfsypsjglpt-jdkp-jqkp_gongan'
            },
            {
                key: 'zfsypsjglpt-jdkp-ajkp_gongan',
                title: '案件考评',
                lic: 'AUDIO_MENU_JDKP_AJKP',
                url: '/zfsypsjglpt-jdkp-ajkp_gongan'
            },
            {
                key: 'zfsypsjglpt-jdkp-kphczl_jiaojing',
                title: '考评抽查总览-交警',
                lic: 'AUDIO_MENU_JDKP_KPCCZL',
                url: '/zfsypsjglpt-jdkp-kphczl_jiaojing'
            },
            {
                key: 'zfsypsjglpt-jdkp-jycx_jiaojing',
                title: '简易程序考评',
                lic: 'AUDIO_MENU_JDKP_JTWFKP_JYCX',
                url: '/zfsypsjglpt-jdkp-jycx_jiaojing'
            },
            {
                key: 'zfsypsjglpt-jdkp-fxccf_jiaojing',
                title: '非现场处罚考评',
                lic: 'AUDIO_MENU_JDKP_JTWFKP_FXCCF',
                url: '/zfsypsjglpt-jdkp-fxccf_jiaojing'
            },
            {
                key: 'zfsypsjglpt-jdkp-qzcs_jiaojing',
                title: '强制措施',
                lic: 'AUDIO_MENU_JDKP_JTWFKP_QZCS',
                url: '/zfsypsjglpt-jdkp-qzcs_jiaojing'
            },
            {
                key: 'zfsypsjglpt-jdkp-sgcl_jiaojing',
                title: '事故处理考评',
                lic: 'AUDIO_MENU_JDKP_SGCLKP',
                url: '/zfsypsjglpt-jdkp-sgcl_jiaojing'
            }
        ]
    },
    {
        key: 'tjfx',
        title: '统计分析',
        icon: 'tjfx',
        lic: 'AUDIO_MENU_TJFX',
        children: [{
                key: 'zfsypsjglpt-tjfx-khqktj_gongan',
                title: '考核情况统计（公安）',
                lic: 'AUDIO_MENU_TJFX_KHQKTJ_GA',
                url: '/zfsypsjglpt-tjfx-khqktj_gongan'
            },
            {
                key: 'zfsypsjglpt-tjfx-khqktj_jiaojing',
                title: '考核情况统计',
                lic: 'AUDIO_MENU_TJFX_KHQKTJ',
                url: '/zfsypsjglpt-tjfx-khqktj_jiaojing'
            },
            {
                key: 'zfsypsjglpt-tjfx-slqktj',
                title: '摄录情况统计',
                lic: 'AUDIO_MENU_TJFX_SLQKTJ_JJ',
                url: '/zfsypsjglpt-tjfx-slqktj'
            },
            {
                key: 'zfsypsjglpt-tjfx-glqktj_gongan',
                title: '关联情况统计-公安',
                lic: 'AUDIO_MENU_TJFX_GLQKTJ_GA',
                url: '/zfsypsjglpt-tjfx-glqktj_gongan'
            },
            {
                key: 'zfsypsjglpt-tjfx-glqktj_jiaojing',
                title: '关联情况统计-交警',
                lic: 'AUDIO_MENU_TJFX_GLQKTJ_JJ',
                url: '/zfsypsjglpt-tjfx-glqktj_jiaojing'
            },
            {
                key: 'zfsypsjglpt-tjfx-zctj',
                title: '资产统计',
                lic: 'AUDIO_MENU_TJFX_ZCTJ_JJ',
                url: '/zfsypsjglpt-tjfx-zctj'
            },
            {
                key: 'zfsypsjglpt-tjfx-4gzfyzxltj',
                title: '4G执法仪在线率统计',
                lic: 'AUDIO_MENU_TJFX_4GZFYZXLTJ',
                url: '/zfsypsjglpt-tjfx-4gzfyzxltj'
            }
        ]
    },
    {
        key: 'zfsypsjglpt-rzgl-index',
        title: '日志管理',
        icon: 'rzgl',
        lic: 'AUDIO_MENU_RZGL',
        children: [],
        url: '/zfsypsjglpt-rzgl-index'
    }
];

// 执法档案
export let zfdaMenu = [{
        key: 'zfsypsjglpt-zfda-jtwf-jycx',
        title: '简易程序',
        lic: 'AUDIO_MENU_ZFDA_JTWF_JYCX',
        children: [],
        url: '/zfsypsjglpt-zfda-jtwf-jycxkp'
    },
    {
        key: 'zfsypsjglpt-zfda-jtwf-fxccf',
        title: '非现场处罚',
        lic: 'AUDIO_MENU_ZFDA_JTWF_FXCCL',
        children: [],
        url: '/zfsypsjglpt-zfda-jtwf-fxccf'
    },
    {
        key: 'zfsypsjglpt-zfda-jtwf-qzcs',
        title: '强制措施',
        lic: 'AUDIO_MENU_ZFDA_JTWF_QZCS',
        children: [],
        url: '/zfsypsjglpt-zfda-jtwf-qzcs'
    }
];

// 监督考评
export let jdkpMenu = [{
        key: 'zfsypsjglpt-jdkp-jycxkp',
        title: '简易程序',
        icon: 'rzgl',
        lic: 'AUDIO_FUNCTION_JDKP_JTWFKP_JYCX',
        children: [],
        url: '/zfsypsjglpt-jdkp-jycxkp'
    },
    {
        key: 'zfsypsjglpt-jdkp-fxccf_jiaojing',
        title: '非现场处罚',
        icon: 'rzgl',
        lic: 'AUDIO_FUNCTION_JDKP_JTWFKP_FXCCF',
        children: [],
        url: '/zfsypsjglpt-jdkp-fxccf_jiaojing'
    },
    {
        key: 'zfsypsjglpt-jdkp-qzcs_jiaojing',
        title: '强制措施',
        icon: 'rzgl',
        lic: 'AUDIO_FUNCTION_JDKP_JTWFKP_QZCS',
        children: [],
        url: '/zfsypsjglpt-jdkp-qzcs_jiaojing'
    }
];