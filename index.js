// 公共引用部分，兼容IE8用 
require('/apps/common/common')
let { applyRouteConfig } = require('/services/pagesRouterServer')
let { mainIndex } = require('/services/configService')
let storage = require('/services/storageService.js').ret
let ajax = require('/services/ajaxService.js')

let userName = storage.getItem('userName')
let roleNames = storage.getItem('roleNames')
let uid = storage.getItem('uid');
avalon.define({
    $id: 'indexVm',
    currentPage: ''
})

//无登录信息时退出并跳转登录页
function isLogin() {
    if (!userName && !roleNames || !uid) {
        storage.clearAll();
        global.location.href = '/main-login.html';
    }
    ajax({
        url: '/gmvcs/uap/shortcut/batch/edit/order',
        method: 'post',
        data: []
    })
}

isLogin()

//router server

/**
 * 首页自定义版本
 * @param mainIndex  mainLine 主线版本  shandongLine 山东 版本
 * 
 */
let routeConfig = null

switch (mainIndex) {
    case 'main_index':
        routeConfig = [
            {
                path: '/',
                component(resolve) {
                    require.async('/apps/mainIndex/mainLine/mainLine', resolve);
                }
            }
        ]
        break
    case 'main_sdjj':
        routeConfig = [
            {
                path: '/',
                component(resolve) {
                    require.async('/apps/mainIndex/shandongLine/shandongLine', resolve);
                }
            }
        ]
        break
}

applyRouteConfig(routeConfig, {
    name: 'indexVm'
})

avalon.history.start({
    fireAnchor: false,
    root: "/", //根路径
    html5: true, //是否使用HTML5 history 
    hashPrefix: ""
})

avalon.scan(document.body)