//
import {
    singleSignOn,
    gt,
    apiUrl,
    copyRight,
    telephone,
    titleName,
    mainIndex
} from '../../services/configService';
require('bootstrap');
require('../../vendor/jquery/jquery.placeholder.min.js');
import ajax from '../../services/ajaxService.js';
import {
    notification
} from "ane";
var storage = require('../../services/storageService.js').ret;

document.title = titleName;

var login_vm = avalon.define({
    $id: 'login_vm',
    currentPage: '',
    user: {},
    username: '',
    password: '',
    loginInfo: {},
    authInfo: {},
    singleSignOn: singleSignOn,
    titleName: titleName,
    copyRight: copyRight,
    telephone: telephone,
    version: '',

    login() {
        if (this.username == "") {
            notification.warn({
                message: '请输入用户名！',
                title: '温馨提示'
            });
            return;
        }

        if (this.password == "") {
            notification.warn({
                message: '请输入密码！',
                title: '温馨提示'
            });
            return;
        }

        ajax({
            url: '/gmvcs/uap/cas/login',
            method: 'post',
            data: {
                "account": $.trim(this.username),
                "password": $.trim(this.password)
            }
        }).then(result => {
            //1404表示未授权，1405表示授权已过期
            if (result.code == 1404 || result.code == 1405) {
                storage.setItem('license', 'none');
                storage.setItem("licenseCode", result.code);
                global.location.href = '/tyywglpt.html#!/tyywglpt-sqgl-index';
                return;
            }
            if (0 != result.code) {
                notification.warn({
                    message: result.msg,
                    title: '温馨提示'
                });
                return;
            }
            this.handleLoginData(result.data);
        });
    },
    // pki登录
    pki_login() {
        let baseUrl = '/gmvcs/uap/cas/pki/login';
        // 单点登录
        if (GetQueryString('usercode')) {
            ajax({
                url: baseUrl + "?userCode=" + GetQueryString('usercode') + "&gt=" + gt,
                method: 'get',
                data: {}
            }).then(result => {
                if (0 == result.code) {
                    this.handleLoginData(result.data);
                }
            });
        } else {
            window.location.href = "http://" + window.location.host + apiUrl + baseUrl;
        }
    },
    handleLoginData(data) {
        // 是否已关闭浏览器下载提示，先保存在browserTipsHadHhow中间变量上，防止登录后清空
        let browserTipsHadShow = false;
        if (!!storage.getItem('browser-tips-had-show')) {
            browserTipsHadShow = storage.getItem('browser-tips-had-show');
        }
        storage.clearAll(); //清除之前的所有数据
        //设置本地储存或cookie > loginInfo
        storage.setItem('account', data.account);
        storage.setItem('userName', data.userName);
        storage.setItem('orgCode', data.orgCode);
        storage.setItem('orgId', data.orgId);
        storage.setItem('orgPath', data.orgPath);
        storage.setItem('uid', data.uid);
        storage.setItem('orgName', data.orgName);
        storage.setItem('roleNames', data.roleNames);
        storage.setItem('roleIds', data.roleIds);
        storage.setItem('lastlogintime', data.lastlogintime);
        storage.setItem('lastLonginIp', data.lastLonginIp);
        storage.setItem('browser-tips-had-show', browserTipsHadShow);


        //根据admin字段判断是否是超级管理员;若false->根据policeType字段判断是普通警员还是领导[true为领导,false为普通警员]
        /*
        LEVAM_JYLB_LD,           // 领导
        LEVAM_JYLB_ZONGDUI_LD,   // 总队领导
        LEVAM_JYLB_ZHIDUI_LD,   // 支队领导
        LEVAM_JYLB_DADUI_LD,    // 大队领导
        LEVAM_JYLB_ZHONGDUI_LD, // 中队领导
        
        LEVAM_JYLB_JY,           // 警员
        LEVAM_JYLB_ZSJY,        // 正式警员
        LEVAM_JYLB_FJ,          // 辅警

        LEVAM_JYLB_FZRY         //法制人员
        LEVAM_JYLB_FZLD         //法制领导

        LEVAM_JYLB_QT           // 其他
        */
        let tempPoliceType = false;
        let kplb_type; //0表示执法类,1表示法制类,2表示admin超级管理员,3表示其他
        if (data.admin) {
            tempPoliceType = true;
            kplb_type = 2;
        } else {
            let resultPolice = data.policeType;
            switch (resultPolice) {
                case "LEVAM_JYLB_LD":
                case "LEVAM_JYLB_ZONGDUI_LD":
                case "LEVAM_JYLB_ZHIDUI_LD":
                case "LEVAM_JYLB_DADUI_LD":
                case "LEVAM_JYLB_ZHONGDUI_LD":
                    tempPoliceType = true;
                    kplb_type = 0;
                    break;
                case "LEVAM_JYLB_JY":
                case "LEVAM_JYLB_FJ":
                    tempPoliceType = false;
                    kplb_type = 0;
                    break;
                case "LEVAM_JYLB_FZRY":
                    tempPoliceType = false;
                    kplb_type = 1;
                    break;
                case "LEVAM_JYLB_FZLD":
                    tempPoliceType = true;
                    kplb_type = 1;
                    break;
                case "LEVAM_JYLB_QT":
                    tempPoliceType = false;
                    kplb_type = 3;
                    break;
                default:
                    tempPoliceType = false;
                    kplb_type = 3;
                    break;
            }
        }

        storage.setItem("policeType", tempPoliceType);
        storage.setItem("kplb_type", kplb_type);

        setTimeout(function () {
            global.location.href = '/';
        }, 0);
    }
});


login_vm.$watch('onReady', function () {
    // 初始化浏览器下载提示localstorage
    storage.setItem('browser-tips-had-show', false);
    //兼容ie8的placeholder无效的问题
    $('input').placeholder();
    ajax({
        url: '/gmvcs/uap/cas/install/version',
        method: 'get',
        data: {}
    }).then(result => {
        if (0 != result.code) {
            return;
        }
        if (0 == result.code) {
            login_vm.version = "当前版本：" + result.data.installVersion;
        }
    });
});

function GetQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r !== null) {
        return (r[2]);
    }
    return null;
}