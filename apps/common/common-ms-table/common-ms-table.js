import './common-ms-table.less'


let unwatch, popoverMove = false;

avalon.component('ms-comtable-header', {
    template: '<th><slot /></th>',
    soleSlot: 'content',
    defaults: {
        content: '',
        col: ''
    }
});


/**
 *  @prop {Array} colwidth 列表宽度
 * 
 */

avalon.component('ms-comtable', {
    soleSlot: 'header',
    template: `<div>
                <div class="common-list-panel" :css="panelCss" :class="[(isdown?'unselect':'')]">
                    <div class="common-list-header-wrap">
                        <ul class="common-list-header" :mouseleave="headermouseleave($event)" :mousemove="headermove($event)" :css="{'margin-left':marginLeft,paddingRight:paddingRight}">
                            <li :for="($index,el) in @columns" :if="!el.isHide" :mousedown="headerdown($event, $index)" :mouseup="headerup($event,$index)" :class="[((($index+1)==columns.length && @ispaddingRight)?'last-item':'')]" :css="{width:el.colwidth}">{{el.title}}</li>
                        </ul>
                    </div>
                    <ul class="common-list-content" :scroll="lisSroll">
                        <li :if="@loading" class="list-loading">
                            <span>结果加载中</span>
                        </li>
                        <li :if="(!@data.length && !@loading)" class="list-null">暂无数据</li>
                        <li :if="!@loading" class="common-list-no-scroll-x" :for="($index, record) in @data">
                            <div :mouseover="popovermouseover($event,key)" :mouseout="popovermouseout($event)" 
                             :for="(key,col) in @columns" :if="!col.isHide" :css="{width:col.colwidth}" :html="col.template"></div>
                        </li>
                    </ul>
                </div>

                <div class="clearfix"></div>
            </div>`,
    defaults: {
        header: '',
        columns: [],
        data: [],
        panelCss: {
            position: 'absolute',
            bottom: 60,
            top: 180,
            left: 0,
            right: 8,
        },
        loading: false,
        onChange: avalon.noop,
        marginLeft: 0,
        paddingRight: 17,
        ispaddingRight: false,
        onInit(event) {
            popoverMove = false;
            let descriptor = getChildTemplateDescriptor(this);
            this.columns = getColumnConfig(descriptor);

            $(document).off('mouseenter').on('mouseenter', '.popover.top.in', function () {
                popoverMove = true;
                $(this).show();
            });

            $(document).off('mouseleave').on('mouseleave', '.popover.top.in', function () {
                popoverMove = false;
                $(this).hide();
            });
        },
        isdown: false,
        pageX: 0,
        clientWidth: 0,
        liindex: 0,
        headerdown(e, index) {
            if (e.target.clientWidth - e.offsetX < 8) {
                this.liindex = index;
                this.pageX = e.pageX;
                this.clientWidth = e.target.offsetWidth;
                this.isdown = true;
            }
        },
        headermove(e) {

            if (e.target.clientWidth - e.offsetX < 8) {
                avalon(e.target).css({
                    'cursor': 'e-resize'
                })
            } else {
                avalon(e.target).css({
                    'cursor': 'auto'
                })
            }

            if (this.isdown) {

                avalon(this.$element).addClass('unselect');
                avalon(e.target).css({
                    'cursor': 'e-resize'
                });

                this.columns[this.liindex].colwidth = this.clientWidth + (e.pageX - this.pageX);

                this.autoHeader(true);
            }
        },
        headerup(e, index) {
            this.isdown = false;
            avalon(e.target).css({
                'cursor': 'auto'
            })
            avalon(this.$element).removeClass('unselect');
        },
        headermouseleave(e) {
            this.headerup(e);
        },
        scrollLeft: 0,
        lisSroll(e) {
            this.scrollLeft = e.target.scrollLeft;
        },
        actions: avalon.noop,
        handle: function (type, col, record, $index) {
            var extra = [];
            for (var _i = 4; _i < arguments.length; _i++) {
                extra[_i - 4] = arguments[_i];
            }
            var text = record[col.dataIndex].$model || record[col.dataIndex];
            this.actions.apply(this, [type, text, record.$model, $index].concat(extra));
        },

        popoverTime: '',
        popovermouseover(e, key) {
            // console.log(e.pageX)
            // console.log(e.pageY)
            if (!this.columns[key].popover) {
                return false
            }

            let targetEle = $(e.target);

            targetEle.popover({
                trigger: 'manual',
                container: 'body',
                placement: 'top',
                html: 'true',
                content: function () {
                    return $(this).text()
                },
                animation: false
            })

            clearTimeout(this.popoverTime);

            this.popoverTime = setTimeout(() => {
                targetEle.popover('show');
            }, 1000);
        },
        isScroll: false,
        autoHeader(isdrap) {
            let content = avalon(this.$element).element.children[0];
            let conH = content.clientHeight;
            let contentLiH = 34 * this.data.length;
            if (this.isScroll) {
                conH -= 23
            }

            if (contentLiH > conH) {
                this.paddingRight = 17;
                this.ispaddingRight = false;
            } else {
                this.paddingRight = 0;
                this.ispaddingRight = true;
            }

            if (isdrap) {
                this.autoHeaderWidth(content.firstChild.firstChild, isdrap);
            } else {
                this.autoHeaderWidth(content.firstChild.firstChild);
            }
        },
        autoHeaderWidth(parentElement, isdrap) {
            let parentElementWidth = parentElement.clientWidth;
            let parentElementChildren = parentElement.children;

            let parentElementChildrenWidth = 0;
            avalon.each(parentElementChildren, (index, item) => {
                parentElementChildrenWidth += avalon(item).width()
            });

            if (parentElementChildrenWidth < (parentElementWidth - this.columns.length)) {
                this.ispaddingRight = false;
                if (isdrap) {
                    this.isScroll = false;
                    // console.log('无横向滚动条')
                }
            } else {
                this.ispaddingRight = true;
                if (isdrap) {
                    this.isScroll = true;
                    // console.log('有横向滚动条')
                }
            }
        },
        popovermouseout(e) {
            clearTimeout(this.popoverTime);
            setTimeout(() => {
                if (popoverMove) {
                    $(e.target).popover('show');
                } else {
                    $(e.target).popover('hide');
                }
            }, 100)
        },
        onReady(event) {
            this.autoHeader();

            unwatch = this.$watch('scrollLeft', val => {
                this.marginLeft = -val;
            });

            this.$watch('data.length', val => {
                this.autoHeader();
            })
        },
        onDispose(vm, el) {
            unwatch();
            clearTimeout(this.popoverTime);
            $(document).off('mouseenter', '.popover.top.in');
            $(document).off('mouseleave', '.popover.top.in');
        }
    }
});

function getChildTemplateDescriptor(vmodel, render) {
    if (render === void 0) {
        render = vmodel.$render;
    }
    if (render.directives === undefined) {
        return [];
    }
    return render.directives.reduce(function (acc, action) {
        if (action.is) {
            acc.push({
                is: action.is,
                props: action.value,
                inlineTemplate: action.fragment,
                children: getChildTemplateDescriptor(vmodel, action.innerRender || {
                    directives: []
                })
            });
        }
        return acc;
    }, []);
}

function getColumnConfig(descriptor, level) {
    if (level === void 0) {
        level = 1;
    }
    return descriptor.reduce(function (acc, column) {
        if (column.is != 'ms-comtable-header')
            return acc;
        if (column.props.type == 'selection') {
            return acc;
        }
        if (column.props.type == 'index') {
            acc.push({
                title: column.props.title,
                dataIndex: '',
                template: '{{$index + 1}}',
                colwidth: column.props.colwidth || '',
                popover: column.props.popover || 0,
                isHide: column.props.isHide || 0,
            });
            return acc;
        }
        var inlineTemplate = column.inlineTemplate;
        inlineTemplate = inlineTemplate.replace(/(ms-|:)skip="[^"]*"/g, '');
        inlineTemplate = inlineTemplate.replace(/<\s*ms-comtable-header[^>]*>.*<\/\s*ms-comtable-header\s*>/g,
            '');
        inlineTemplate = inlineTemplate.replace(/(ms-|:)click="handle\(([^"]*)\)"/g, function ($0, $1,
            $2, $3) {
            return ($1 + "click=\"handle(" + $2 + ",)\"").replace(/,/, ', col, record, $index,')
                .replace(/,\)/, ')');
        });
        acc.push({
            title: column.props.title,
            dataIndex: column.props.dataIndex || '',
            template: /^\s*$/.test(inlineTemplate) ? '{{record.' + column.props.dataIndex +
                '}}' : inlineTemplate,
            colwidth: column.props.colwidth || '',
            popover: column.props.popover || 0,
            isHide: column.props.isHide || 0,
        });
        return acc.concat(getColumnConfig(column.children, level + 1));
    }, []);
}