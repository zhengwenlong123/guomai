import avalon from 'avalon2';

import * as menuService from '../../services/menuService';
import { menu as menuStore } from '../../services/storeService';
import 'ane';

avalon.effect('collapse', {
    enter(elem, done) {
        $(elem).slideDown(200, done);
    },
    leave(elem, done) {
        $(elem).slideUp(200, done);
    }
});

export const name = 'common-sidebar';

avalon.component(name, {
    template: __inline('./common-sidebar.html'),
    defaults: {
        menu: [],
        selectedKeys: ['tyyhrzpt-xtpzgl-yhgl'],
        openKeys: [],
        handleMenuClick(item, key, keyPath) {
            avalon.history.setHash(item.uri);
        },
        handleOpenChange(openKeys) {
            this.openKeys = openKeys.slice(-1);
        },
        onInit(event) {
            menuService.menu.then((menu) => {   
                this.menu = menu.CAS_MENU_TYYHRZPT;
                if(global.location.hash == '#!/') {
                    let firstSelectedNav = this.menu[0].uri;
                    avalon.history.setHash(firstSelectedNav);  // 默认选中第一项菜单
                }
                let hash = global.location.hash;
                let keyPath = hash.replace("#!/", "");
                this.selectedKeys = "" == keyPath ? ["tyyhrzpt-xtpzgl-yhgl"] : [keyPath];
            });
            menuStore.selectedKeys$.subscribe(v => {
                let hash = global.location.hash;
                let keyPath = hash.replace("#!/", "");
                this.selectedKeys = "" == keyPath ? ["tyyhrzpt-xtpzgl-yhgl"] : [keyPath];
                //this.selectedKeys = v;
            });
            menuStore.openKeys$.subscribe(v => {
                let hash = global.location.hash;
                let keyPath = hash.replace("#!/", "");
                this.openKeys = "" == keyPath ? ["tyyhrzpt-xtpzgl-yhgl"] : [keyPath];
                //this.openKeys = v;
            });
        }
    }
});