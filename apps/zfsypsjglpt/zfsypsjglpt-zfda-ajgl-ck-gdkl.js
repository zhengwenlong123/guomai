import avalon from 'avalon2';
import './zfsypsjglpt-zfda-ajgl-ck-gdkl.css'


export const name = 'zfsypsjglpt-zfda-ajgl-ck-gdkl'

let ajglGdkl = avalon.component(name, {
    template: __inline('./zfsypsjglpt-zfda-ajgl-ck-gdkl.html'),
    defaults: {
        loading: false,
        data: [{
            checked: false,
            aa: 11
        }, {
            checked: false,
            aa: 1222
        }, {
            checked: false,
            aa: 333
        }],
        // 全选config
        checkboxAllConfig: {
            checked: false
        },
        // 全选回调
        checkboxAllChange(configData) {
            avalon.each(this.data, (index, item) => {
                item.checked = configData.checked

            })
        },
        // list 单选回调
        checkboxChange(configData) {
            console.log(configData);
        }
    }
})

ajglGdkl.extend({
    displayName: 'ms-gdkl-checkbox',
    template: `<i class="fa" :css="[checkboxStyle,(config.checked?checkStyle:uncheckStyle)]" :click="@chage | stop | prevent" :class="[(config.checked?'fa-check-square checked':'fa-square-o')]"></i>`,
    defaults: {
        onInit(e) {},
        config: {},
        checkboxStyle: {
            cursor: 'pointer',
            fontSize: '16px'
        },
        checkStyle: {
            color: '#0078d7'
        },
        uncheckStyle: {
            color: '#cccccc'
        },
        onChang: avalon.noop,
        chage(event) {
            try {
                if (this.config.hasOwnProperty('checked')) {
                    this.config.checked = !this.config.checked
                    this.onChang(this.config)
                } else {
                    throw 'err'
                };
            } catch (err) {
                avalon.error('ms-gdkl-checkbox组件 config 参数未传入checked');
            }
        }
    }
})