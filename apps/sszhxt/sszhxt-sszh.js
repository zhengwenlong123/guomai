import * as avalon from 'avalon2';
import {
    createForm,
    notification
} from 'ane';
import {
    isAllowSpeak,
    isAllowVideo,
    isAllowRecord,
    isAllowLock,
    isAllowPhotograph,
    mapType,
    gxxOcxVersion
} from '../../services/configService';
import ajax from '../../services/ajaxService';
import {
    Gxxplayer
} from "/vendor/gosunocx/gosunocx";
//import {mapInitObj} from '../common/common-mapInit';
const storage = require('../../services/storageService.js').ret;
// import io from 'socket.io-client';
require("../common/common-sszh-voiceTool.js");
require("./sszhxt-sszh.css");
export const name = 'sszhxt-sszh';
let player, ocxele; //播放器
let updatemarkerArr = new Array; //地图更新标签数组
let DuoupdatemarkerArr = new Array; //地图多通道更新标签数组
let speakPerson = {}; //语音窗口当前对讲的人
let updatemarkerTime;
let vm = null,
    zTreeObj;
// var latitude = 23.00;
// var longitude = 113.27;
var longinPersonOrgpath, longinPersonUid; //保存当前登录用户的部门path,用于sos对比部门
var sszh = avalon.component(name, {
    template: __inline('./sszhxt-sszh.html'),
    defaults: {
        isie: '',
        recentData: [],
        downloadTipShow: false,
        tipText: '当前页面需要高新兴视频播放器插件支持',
        showtip:true,
        // showmap(e){
        //     if(e.keyCode == 13){
        //         $('.sszhxt-sszh-map').show();
        //     }
        //
        // },
        // hidemap(e){
        //     if(e.keyCode == 13){
        //         $('.sszhxt-sszh-map').hide();
        //     }
        //
        // },
        getShowStatus(show) {
            vm.downloadTipShow = show;
        },
        handleTreeCheck(event, treeId, treeNode, center) {
            if (treeNode.checked == false) {
                removerUpdatemarkerArr(treeNode.gbcode);
                if (sszhdeviceInfo.gbcode == treeNode.gbcode) {
                    sszhdeviceInfo.visible = false;
                }

                return;
            }
            let pData, url;
            //分执法仪和多通道设备
            if (treeNode.mytype == 0) {
                pData = {
                    'devices': [treeNode.gbcode],
                    'deviceType': "DSJ"
                };
                url = '/gmvcs/instruct/mapcommand/devicegps';
            } else {
                pData = [treeNode.gbcode]
                url = '/gmvcs/uom/device/listByGbcodeList?attachChannelInfo=true';
            }

            ajax({
                url: url,
                method: 'post',
                data: pData
            }).then(result => {
                if (result.code != 0) {
                    notification.warn({
                        title: '通知',
                        message: "获取设备信息失败"
                    });
                    return;
                }
                // result.data[treeNode.gbcode].latitude = 24.12;
                // result.data[treeNode.gbcode].longitude = 113.12;
                //多通道设备的接口放回值不一致，处理
                if (treeNode.mytype != 0) {
                    // result.data[treeNode.gbcode] = result.data[treeNode.gbcode][0];
                    result.data[treeNode.gbcode].source = result.data[treeNode.gbcode].platformGbcode;
                }
                if (!result.data[treeNode.gbcode].latitude || !result.data[treeNode.gbcode].longitude) {
                    if (!center) {
                        notification.warn({
                            title: '通知',
                            message: "部分设备无GPS"
                        });
                        return;
                    }
                    // result.data[treeNode.gbcode].latitude = latitude;
                    // result.data[treeNode.gbcode].longitude = longitude;
                    // latitude+=0.02;
                    // longitude+=0.02;
                    sszhdeviceInfo.gbcode = treeNode.gbcode;
                    if (result.data[treeNode.gbcode].userName == "") {
                        sszhdeviceInfo.username = '';
                    } else {
                        sszhdeviceInfo.username = result.data[treeNode.gbcode].userName;
                    }
                    if (result.data[treeNode.gbcode].userCode == "") {
                        sszhdeviceInfo.usercode = '-';
                    } else {
                        sszhdeviceInfo.usercode = result.data[treeNode.gbcode].userCode;
                    }
                    if (!result.data[treeNode.gbcode].capacityUsed) {
                        sszhdeviceInfo.capacityUsed = 0;
                    } else {
                        sszhdeviceInfo.capacityUsed = result.data[treeNode.gbcode].capacityUsed;
                    }
                    if (!result.data[treeNode.gbcode].capacityTotal) {
                        sszhdeviceInfo.capacityTotal = 0;
                    } else {
                        sszhdeviceInfo.capacityTotal = result.data[treeNode.gbcode].capacityTotal;
                    }
                    if (!result.data[treeNode.gbcode].battery) {
                        sszhdeviceInfo.battery = 0;
                    } else {
                        sszhdeviceInfo.battery = Number(result.data[treeNode.gbcode].battery);
                    }
                    if (!result.data[treeNode.gbcode].source) {
                        sszhdeviceInfo.source = false;
                    } else {
                        sszhdeviceInfo.source = true;
                    }
                    //type只有多通道，外域执法仪和多通道设备都是显示一样的
                    sszhdeviceInfo.devName = result.data[treeNode.gbcode].name || result.data[treeNode.gbcode].deviceName;
                    sszhdeviceInfo.devmodel = result.data[treeNode.gbcode].model;
                    //这个表示外域执法仪
                    if (treeNode.mytype == 0) {
                        sszhdeviceInfo.type = '执法仪';
                    }
                    if (treeNode.mytype != 0) {
                        sszhdeviceInfo.type = result.data[treeNode.gbcode].type;
                        sszhdeviceInfo.gbcodeArr = result.data[treeNode.gbcode].channelSet;
                    }
                    sszhdeviceInfo.mytype = treeNode.mytype;
                    sszhdeviceInfo.signal = result.data[treeNode.gbcode].signal == undefined ? 0 : result.data[treeNode.gbcode].signal;
                    sszhdeviceInfo.lockStatus = result.data[treeNode.gbcode].locked == undefined ? 0 : result.data[treeNode.gbcode].locked;
                    sszhdeviceInfo.videoStatus = result.data[treeNode.gbcode].videoStatus;
                    if (sszhdeviceInfo.videoStatus == 1 && sszhdeviceInfo.$recordArr.indexOf(sszhdeviceInfo.gbcode) == -1) {
                        sszhdeviceInfo.$recordArr.push(sszhdeviceInfo.gbcode);
                    }
                    sszhdeviceInfo.visible = true;
                    sszhdeviceInfo.isAllowRecord = isAllowRecord;
                    sszhdeviceInfo.isAllowPhotograph = isAllowPhotograph;
                    sszhdeviceInfo.isAllowLock = isAllowLock;
                    sszhdeviceInfo.isAllowSpeak = isAllowSpeak;
                    return;
                }
                //保存一个坐标点，用于缩小地图使用
                vm.lon = result.data[treeNode.gbcode].longitude;
                vm.lat = result.data[treeNode.gbcode].latitude;

                avalon.each(result.data, function (index, item) {
                    item.gbcode = treeNode.gbcode;
                    //mytype表示自定义设备类型0 执法仪， 1：快速 2：车 3:无人机
                    item.mytype = treeNode.mytype;
                })
                createUpdatemarkerArr(result.data[treeNode.gbcode], center);
            });
        },
        extraProcessWhenExpand(i, value) {
            isShowMap(i, value); //是否在地图上有标记点
        },
        extraProcessWhenPersonChange(node) {
            removerUpdatemarkerArr(node.gbcode); //不在线清楚地图标注
        },
        extraHandleWhenCheckOrg() {
            if (vm.lon == '' || vm.lat == '') {
                return;
            }
            $('#mapIframe')[0].contentWindow.esriMap.setMapCenter(vm.lon, vm.lat, 8);
        },
        returnTreeObj(a) {
            zTreeObj = a;
        },
        handleDeviceClick(el, event) { //点击最近人员设备
            if (el.commandType == 'USER') {
                return;
            }
            el.checked = null;
            this.handleTreeCheck(event, '', el);
        },
        handlePersonDeviceClick(item, event) {
            let obj = {};
            obj.gbcode = item;
            obj.checked = null;
            this.handleTreeCheck(event, '', obj);
        },
        lon: '',
        lat: '',
        sszhmapobj: '',
        toolUse: true,
        setcity() {
            sszhmap.showcityName = avalon.components['common-sszh-mapcity'].defaults.nowcity;
            this.lon = avalon.components['common-sszh-mapcity'].defaults.lon;
            this.lat = avalon.components['common-sszh-mapcity'].defaults.lat;
            //保存当前点击的城市
            avalon.vmodels['sszhxt_vm'].$cityDetailobj.nowClickcity = sszhmap.showcityName;
        },
        ocxindex: 1, //ocx窗口值
        //音量调节
        handleSoundLevel(v) {
            //须先开启声音
            player.SoundCtrl(1, 1, 1);
            player.setVolume(v);
        },
        onInit(event) {
            vm = event.vmodel;
            vm.isie = isIE();
            window._onOcxEventProxy = _onOcxEventProxy;
            $('.sszhxt-sszh-map').show();

        },
        onReady() {
            //用户设置的默认城市
            sszhmap.showcityName = avalon.vmodels['sszhxt_vm'].$cityDetailobj.nowClickcity;
            this.lon = avalon.vmodels['sszhxt_vm'].$cityDetailobj.lon;
            this.lat = avalon.vmodels['sszhxt_vm'].$cityDetailobj.lat;
            updatemarkerTime = setInterval(function () {
                refreshTimed(); //地图点更新函数
            }, 5000);
            if ($('#mapIframe')[0].contentWindow.esriMap) {
                $('#mapIframe')[0].contentWindow.esriMap.remove_overlay();
                $('#mapIframe')[0].contentWindow.esriMap.removeTrackLayer();
            }
            //======================


            //=======================
            let data = storage.getItem('sszhxt-SOS');
            if (data) this.handleTreeCheck('', '', data, true);
            // setInterval(function () {
            //     let z =  Number($("#regOcxDiv1").attr('z-index')) || 0;
            //     $("#regOcxDiv1").attr('z-index',  z +100 );
            //     console.log('z-index',  z);
            // }, 100);
            initOcx();

        },
        onDispose() {
            $('.mapcity_popup_main').hide();
            updatemarkerArr = [];
            if (vm.isie && ocxele.object || !vm.isie && undefined != ocxele.GS_ReplayFunc) { //防止没有安装ocx报错
                //清除掉ocx点流的所有操作
                if (speakPerson.gbcode) {
                    sszhyyth.closesszhhyyth();
                }
                if (0 != player.getStatusByIndex(-1)) {
                    player.stopRec(1); //结束视频
                    player.stopTalk(sszhsp.gbcode); //结束音频
                }
            }
            avalon.each(sszhmap.devhtmllist, function (index, item) {
                clearInterval(item.timeobj); //清除定时器
            });
            clearInterval(updatemarkerTime); //地图更新定时器
            //停止所有录像
            stopRecordByarr(getRecordDevice());
            //sszhgjxxManage.gjxxlist = [];
            //退出的时候先清除地图所有标记
            $('#mapIframe')[0].contentWindow.esriMap.remove_overlay();
            storage.removeItem('sszhxt-SOS');
            sszhdeviceInfo.visible = false;
        }
    }
});
//无gps定位信息
var sszhdeviceInfo = avalon.define({
    $id: 'sszhdeviceInfo',
    source: false,
    visible: false,
    username: '',
    usercode: '',
    gbcode: '',
    signal: '',
    battery: 0,
    mytype: '',
    capacityUsed: 0,
    capacityTotal: 0,
    lockStatus: 0,
    videoStatus: 0,
    disabled: '',
    $recordArr: [],
    width: '',
    usedWidth: 0,
    spanTwowidth: 0,
    syrl: 0,
    zrl: 0,
    lockword: '',
    type: '', //设备类型名字
    devName: '', //设备名称
    devmodel: '', //外域设备显示设备型号
    gbcodeArr: '', //通道数组
    isAllowRecord: '', //允许录像
    isAllowLock: '', //允许锁定
    isAllowPhotograph: '', //允许拍照
    isAllowSpeak:'',//允许语音
    $computed: {
        width: function () {
            return this.battery / 100 * 29
        },
        usedWidth: function () {
            if (this.capacityUsed && this.capacityTotal) {
                return this.capacityUsed / this.capacityTotal * 180;
            }
            return 0;
        },
        spanTwowidth: function () {
            if (this.usedWidth != 0) {
                return this.usedWidth / 4 + 80;
            } else {
                return 90;
            }
        },
        lockword: function () {
            if (this.lockStatus == 0) {
                return '远程锁定';
            } else {
                return '远程解锁';
            }
        },
        disabled: function () {
            if (this.lockStatus == 0) {
                return '';
            } else {
                return 'disabled';
            }
        }
    },
    hideInfo() {
        this.visible = false;
    },
    record() {
        let data = {};
        let optCmd;
        if (this.videoStatus == 0) {
            optCmd = 'Start';
        } else {
            optCmd = 'Stop';
        }

        ajax({
            url: '/gmvcs/uom/device/dsj/control/record?deviceId=' + sszhdeviceInfo.gbcode + '&optCmd=' + optCmd,
            method: 'post',
            data: null
        }).then(result => {
            if (result.code != 0) {
                notification.error({
                    title: '通知',
                    message: '服务器异常,操作失败'
                });
                return;
            }
            if (optCmd == 'Start') {
                notification.success({
                    title: '通知',
                    message: '录像开始指令下发成功'
                });
                sszhdeviceInfo.videoStatus = 1;
                if (sszhdeviceInfo.$recordArr.indexOf(sszhdeviceInfo.gbcode) == -1) {
                    sszhdeviceInfo.$recordArr.push(sszhdeviceInfo.gbcode);
                }
            } else {
                notification.success({
                    title: '通知',
                    message: '录像停止指令下发成功'
                });
                sszhdeviceInfo.videoStatus = 0;
                if (sszhdeviceInfo.$recordArr.indexOf(sszhdeviceInfo.gbcode) != -1) {
                    sszhdeviceInfo.$recordArr.splice(sszhdeviceInfo.$recordArr.indexOf(sszhdeviceInfo.gbcode), 1);
                }
            }
        })
    },
    vedioplay() {
        sszhinfowindow.playVideo(this.gbcode, this.username, this.usercode, this.signal, this.devName, this.mytype, this.gbcodeArr);
    },
    startTalk(){
        sszhinfowindow.startTalk(this.gbcode, this.username, this.usercode, this.signal,this.mytype);
    },
    photograph() {
        let data = {};
        ajax({
            url: '/gmvcs/uom/device/dsj/control/photo?deviceId=' + sszhdeviceInfo.gbcode,
            method: 'post',
            data: null
        }).then(result => {
            if (result.code != 0) {
                notification.error({
                    title: '通知',
                    message: '服务器异常,拍照指令下发失败'
                });
                return;
            }
            notification.success({
                title: '通知',
                message: '拍照指令下发成功'
            });
        });
    },
    lock() {
        let data = {};
        let optCmd;
        if (this.lockStatus == 0) {
            optCmd = 'Lock';
        } else {
            optCmd = 'Unlock';
        }
        ajax({
            url: '/gmvcs/uom/device/dsj/control/lock?deviceId=' + sszhdeviceInfo.gbcode + '&optCmd=' + optCmd,
            method: 'post',
            data: null
        }).then((ret) => {
            if (ret.code == 0) {
                if (optCmd == "Lock") {
                    notification.success({
                        title: '通知',
                        message: "锁定成功"
                    });
                    sszhdeviceInfo.lockStatus = 1;
                    sszhdeviceInfo.disabled = 'disabled';
                    changeTreeImg(sszhdeviceInfo.gbcode, true);
                    return;
                } else {
                    notification.success({
                        title: '通知',
                        message: "解锁成功"
                    });
                    sszhdeviceInfo.lockStatus = 0;
                    sszhdeviceInfo.disabled = '';
                    changeTreeImg(sszhdeviceInfo.gbcode, false);
                    return;
                }

            }
            notification.error({
                title: '通知',
                message: "服务器后端异常,锁定失败"
            });
        })
    }
})

//判断是否有设备正在录像
function getRecordDevice() {
    var deviceobject = $('#mapIframe')[0].contentWindow.esriMap.getRecordData();
    var arr = [];
    avalon.each(deviceobject, function (key, value) {
        if (value.record == true || value.record == 'true') {
            arr.push(key);
        }
    });
    arr = arr.concat(sszhdeviceInfo.$recordArr);
    if (arr.length <= 0) return;
    return arr;
}
//循环停止录像
function stopRecordByarr(arr) {
    avalon.each(arr, function (index, val) {
        ajax({
            url: '/gmvcs/uom/device/dsj/control/record?deviceId=' + val + '&optCmd=Stop',
            method: 'post',
            data: null
        }).then(result => {
            if (result.code != 0) {
                return;
            }
        });
    })
}

function isIE() {
    if (!!window.ActiveXObject || "ActiveXObject" in window)
        return true;
    else
        return false;
}

function isShowMap(i, value) {
    for (var j = 0, len = updatemarkerArr.length; j < len; j++) {
        if (updatemarkerArr[j].gbcode == value.gbcode) {
            value.checked = true;
            return;
        } else {
            value.checked = false;
            return;
        }
    }

    return false;
}

//操作标记数组的函数
function controlMarkerArr(marker) {
    for (var i = 0; i < updatemarkerArr.length; i++) {
        let markerTmp = updatemarkerArr[i];
        if (markerTmp.gbcode == marker.gbcode) {
            updatemarkerArr.splice(i--, 1);
            updatemarkerArr.push(marker); //将点击后的地图标记更新点放在最后面
            break;
        }
    }
}
/*
 *  新建地图更新点
 * */
function createUpdatemarkerArr(data, center) {
    removerUpdatemarkerArr(data.gbcode); //这里是为了防止它从最近人员那里点击
    //initMapObject.createMarker(data, center);
    //语音对讲是否出现
    data.isAllowSpeak = isAllowSpeak;
    data.isAllowVideo = isAllowVideo;
    data.isAllowRecord = isAllowRecord;
    data.isAllowLock = isAllowLock;
    data.isAllowPhotograph = isAllowPhotograph;
    $('#mapIframe')[0].contentWindow.esriMap.createMarker(data, center);
    let marker = {};
    // marker.dev = data.deviceId;
    marker.gbcode = data.gbcode;
    //执法仪的
    if (data.mytype == 0) {
        updatemarkerArr.push(marker); //将地图点加入更新数组
    } else {
        //多通道的
        DuoupdatemarkerArr.push(marker);
    }

}

/**
 * 删除更新标记数组的单个点
 */
function removerUpdatemarkerArr(gbcode) {
    var markerTmp;
    //执法仪的
    for (var i = 0; i < updatemarkerArr.length; i++) {
        markerTmp = updatemarkerArr[i];
        if (markerTmp.gbcode == gbcode) {
            //initMapObject.removerMarker(gbcode);
            $('#mapIframe')[0].contentWindow.esriMap.removerMarker(gbcode);
            updatemarkerArr.splice(i--, 1);
            break;
        }
    }
    //多通到的
    for (var i = 0; i < DuoupdatemarkerArr.length; i++) {
        markerTmp = DuoupdatemarkerArr[i];
        if (markerTmp.gbcode == gbcode) {
            //initMapObject.removerMarker(gbcode);
            $('#mapIframe')[0].contentWindow.esriMap.removerMarker(gbcode);
            DuoupdatemarkerArr.splice(i--, 1);
            break;
        }
    }
}
/*
 * 更新标记数组的点
 * */
function updateMapMarkers(data) {
    let arr = [];
    arr = updatemarkerArr.concat([]);
    arr.concat(DuoupdatemarkerArr);
    for (let i = 0; i < arr.length; i++) {
        for (let j = 0; j < data.length; j++) {
            if (arr[i].gbcode != undefined && data[j].gbcode != undefined && arr[i].gbcode == data[j].gbcode && data[i].latitude && data[i].longitude) {
                removerUpdatemarkerArr(data[i].gbcode);
                createUpdatemarkerArr(data[i], false);
                break;
            }
        }
    }
}
/*
 *获取更新点的数据
 * duo表示是否是更新多通道的点
 * */
function getUpdateMarkerData(devArr, duo) {
    if (devArr.length == 0) {
        return;
    }
    let pData, url;
    if (!duo) {
        pData = {
            'devices': devArr,
            'deviceType': "DSJ"
        };
        url = '/gmvcs/instruct/mapcommand/devicegps';
    } else {
        pData = devArr;
        url = '/gmvcs/uom/device/listByGbcodeList?attachChannelInfo=true';
    }

    ajax({
        url: url,
        method: 'post',
        data: pData
    }).then(result => {
        if (result.code != 0) {
            notification.warn({
                title: '通知',
                message: "获取设备信息失败"
            });
            return;
        }
        let arr = [];
        if (!duo) {
            for (var i = 0; i < devArr.length; i++) { //按照我自己想要的顺序更新点，不能用avalon.each，这个不按照顺序输出的
                var key = devArr[i];
                avalon.each(updatemarkerArr, function (i, el) {
                    if (result.data[key].deviceId == el.gbcode) {
                        result.data[key].gbcode = el.gbcode;
                        //mytype表示自定义设备类型0 执法仪， 1：快速 2：车 3:无人机
                        result.data[key].mytype = 0;
                        arr.push(result.data[key]);
                    }
                })
            }
        } else {
            for (var i = 0; i < devArr.length; i++) { //按照我自己想要的顺序更新点，不能用avalon.each，这个不按照顺序输出的
                var key = devArr[i];
                //mytype表示自定义设备类型0 执法仪， 1：快速 2：车 3:无人机
                if (result.data[key].type == '快速布控') {
                    result.data[key].mytype = 1;
                } else if (result.data[key].type == '移动车载终端') {
                    result.data[key].mytype = 2;
                } else {
                    result.data[key].mytype = 3;
                }
                arr.push(result.data[key]);
            }
        }
        if (arr.length <= 0) return;
        // arr[0].latitude = latitude;
        // arr[0].longitude = longitude;
        // latitude+=0.02;
        // longitude+=0.02;
        updateMapMarkers(arr, true);
        avalon.each(sszhmap.devhtmllist, function (index, item) {
            avalon.each(result.data, function (i, el) {
                if (!el.signal) el.signal = 0;
                if (speakPerson.gbcode && speakPerson.gbcode == el.gbcode) {
                    sszhyyth.signal = el.signal; //语音窗口signal
                }
                if (item.gbcode == el.gbcode) {
                    item.signal = el.signal; //更新视频窗口信号强度
                    return;
                }
            })

        });
    })
}
/*
 * 获取多通道更新点的数据
 * */
function getUpdateMarkerDataDuo(devArr) {
    if (devArr.length == 0) {
        return;
    }
    ajax({
        url: '/gmvcs/uom/device/listByGbcodeList?attachChannelInfo=false',
        method: 'post',
        data: devArr
    }).then(result => {
        if (result.code != 0) {
            notification.warn({
                title: '通知',
                message: "获取多通道设备信息失败"
            });
            return;
        }


    })
}

/*
 *  定时刷新点调用函数
 * */
function refreshTimed() {
    let postdevArr = [];
    avalon.each(updatemarkerArr.slice(0, updatemarkerArr.length), function (i, v) {
        postdevArr.push(v.gbcode);
    })
    let postdevArrDuo = [];
    avalon.each(DuoupdatemarkerArr.slice(0, DuoupdatemarkerArr.length), function (i, v) {
        DuoupdatemarkerArr.push(v.gbcode);
    })
    getUpdateMarkerData(postdevArr);
    //多通道更新
    getUpdateMarkerData(postdevArrDuo, true);
}

/*
 *  改变部门树图标样式函数
 */
function changeTreeImg(gbcode, symbol) {
    var node = zTreeObj.getNodesByParam("gbcode", gbcode, null)
    if (node) {
        symbol ? node[0].icon = '/static/image/sszhxt/locked.png' : node[0].icon = '/static/image/sszhxt/device_online.png';
    }
    zTreeObj.updateNode(node[0]);
}

//========================================地图======================================
let sszhmap = avalon.define({
    $id: 'sszhmap',
    sszhthmintitletoggle: false,
    sszhyyhtml: [],
    devhtmllist: [],
    //sszhthmintitle : this.sszhyyhtml + this.sszhsphtml,
    expandsszhyy: function (e) {
        $(".sszhyythmincontianer").hide(100, function () {
            $("#sszhyyth").show(300);
        });
    },
    locate(item) { //点击语音地图定位
        let pData, url;
        //分执法仪和多通道设备
        if (item.mytype == 0) {
            pData = {
                'devices': [item.gbcode],
                'deviceType': "DSJ"
            };
            url = '/gmvcs/instruct/mapcommand/devicegps';
        } else {
            pData = [item.gbcode]
            url = '/gmvcs/uom/device/listByGbcodeList?attachChannelInfo=true';
        }
        ajax({
            url: url,
            method: 'post',
            data: pData
        }).then(result => {
            if (result.code != 0) {
                notification.warn({
                    title: '通知',
                    message: "获取设备信息失败"
                });
                return;
            }
            //多通道设备的接口放回值不一致，处理
            //mytype表示自定义设备类型0 执法仪， 1：快速 2：车 3:无人机
            if (item.mytype != 0) {
                // result.data[item.gbcode] = result.data[item.gbcode][0];
                result.data[item.gbcode].source = result.data[item.gbcode].platformGbcode;
            }
            if (!result.data[item.gbcode].latitude || !result.data[item.gbcode].longitude) {

                sszhdeviceInfo.gbcode = item.gbcode;
                if (result.data[item.gbcode].userName == "") {
                    sszhdeviceInfo.userame = '-';
                } else {
                    sszhdeviceInfo.username = result.data[item.gbcode].userName;
                }
                if (result.data[item.gbcode].userCode == "") {
                    sszhdeviceInfo.usercode = '-';
                } else {
                    sszhdeviceInfo.usercode = result.data[item.gbcode].userCode;
                }
                if (!result.data[item.gbcode].capacityUsed) {
                    sszhdeviceInfo.capacityUsed = 0;
                } else {
                    sszhdeviceInfo.capacityUsed = result.data[item.gbcode].capacityUsed;
                }
                if (!result.data[item.gbcode].capacityTotal) {
                    sszhdeviceInfo.capacityTotal = 0;
                } else {
                    sszhdeviceInfo.capacityTotal = result.data[item.gbcode].capacityTotal;
                }
                if (!result.data[item.gbcode].battery) {
                    sszhdeviceInfo.battery = 0;
                } else {
                    sszhdeviceInfo.battery = result.data[item.gbcode].battery;
                }
                if (!result.data[item.gbcode].source) {
                    sszhdeviceInfo.source = false;
                } else {
                    sszhdeviceInfo.source = true;
                }

                //type只有多通道，外域执法仪和多通道设备都是显示一样的
                sszhdeviceInfo.devName = result.data[item.gbcode].name || result.data[item.gbcode].deviceName;
                sszhdeviceInfo.devmodel = result.data[item.gbcode].model;
                //这个表示外域执法仪
                //mytype表示自定义设备类型0 执法仪， 1：快速 2：车 3:无人机
                if (item.mytype == 0) {
                    sszhdeviceInfo.type = '执法仪';
                }
                if (item.mytype != 0) {
                    sszhdeviceInfo.type = result.data[item.gbcode].type;
                    sszhdeviceInfo.gbcodeArr = result.data[item.gbcode].channelSet;
                }
                sszhdeviceInfo.mytype = item.mytype;
                sszhdeviceInfo.signal = result.data[item.gbcode].signal == undefined ? 0 : result.data[item.gbcode].signal;
                sszhdeviceInfo.lockStatus = result.data[item.gbcode].locked == undefined ? 0 : result.data[item.gbcode].locked;
                sszhdeviceInfo.videoStatus = result.data[item.gbcode].videoStatus;
                if (sszhdeviceInfo.videoStatus == 1 && sszhdeviceInfo.$recordArr.indexOf(sszhdeviceInfo.gbcode) == -1) {
                    sszhdeviceInfo.$recordArr.push(sszhdeviceInfo.gbcode);
                }
                sszhdeviceInfo.visible = true;
                sszhdeviceInfo.isAllowRecord = isAllowRecord;
                sszhdeviceInfo.isAllowPhotograph = isAllowPhotograph;
                sszhdeviceInfo.isAllowLock = isAllowLock;
                sszhdeviceInfo.isAllowSpeak = isAllowSpeak;
                return;
            }
            avalon.each(result.data, function (index, el) {
                el.gbcode = item.gbcode;
                el.mytype = item.mytype;
            })
            createUpdatemarkerArr(result.data[item.gbcode], true);
        })
    },
    expandsszhsp(item) {
        sszhsp.hidesszhspth(); //将之前的的人hide，显示在左侧栏
        sszhsp.sszhspthtime = item.time;
        // sszhsp.dev = item.dev;
        sszhsp.gbcode = item.gbcode;
        sszhsp.signal = item.signal;
        if (item.mytype != 0) {
            sszhsp.name = item.name;
            sszhsp.deviceName = item.name;
            if (item.mytype == 1) {
                sszhsp.deviceType = '快速布控设备';
            } else if (item.mytype == 2) {
                sszhsp.deviceType = "车载移动执法设备";
            } else {
                sszhsp.deviceType = "警用无人机";
            }

        } else {
            if (!item.username) {
                sszhsp.name = "与" + item.name + "视频通话中";
            } else {
                sszhsp.name = "与" + item.username + '(' + item.usercode + ')' + "视频通话中";
            }
        }
        // sszhsp.name = "与" + item.username + '(' + item.usercode + ')' + "视频通话中";
        sszhsp.playVideo(item.childGbcode);
        sszhsp.checkItem = item.childGbcode;
        sszhsp.showsszhspth = true;
        //不显示工具
        if (item.mytype != 0) {
            sszhsp.isShowTools = true;
            sszhsp.gbcodeArr = item.devArr;
            $(".sszhspth").css({
                'z-index': 9999,
                'width': 1000,
                'height': '500'
            });
        } else {
            sszhsp.isShowTools = false;
            $(".sszhspth").css({
                'z-index': 9999,
                'width': '700',
                'height': '500'
            });
        }
        $('#npGSVideoPlugin').css({
            'z-index': 999
        });
        //隐藏对应的左侧栏
        avalon.each(this.devhtmllist, function (index, item) {
            if (item.gbcode == sszhsp.gbcode) {
                item.show = false;
            }
        });
    },
    loadingtoggle: true,
    toggleshow: function () {
        $(".sszhgj").hide(100, function () {
            $(".sszhgjxx").show(500); //用avalon的animate会出现切换导航的过程中触发enter,leave动画，换为jquery。
        });

    },
    togglehide: function () {
        $(".sszhgjxx").hide(500, function () {
            $(".sszhgj").show(100);
        });
    },
    move: function (e) {
        let d = e.target.offsetParent; //父元素
        let w = d.scrollWidth; //父元素宽度
        let h = d.offsetHeight;
        dargObject.move(d, e, w, h);
    },
    showtool() {
        $(".mapcljl").toggle(100);
    },
    showcitylist() {
        $('.mapcity_popup_main').toggle(200);
    },
    mearsurelength() {
        //initMapObject.measureLength();
        $('#mapIframe')[0].contentWindow.esriMap.measureLength();
    },
    mapsearchvalue: '请输入警员姓名/警号/设备名称', //地图搜索
    placeholderstatue: 0,
    showmapclose: false,
    $computed: {
        showmapclose: function () {
            if (this.placeholderstatue != 0) {
                return true;
            } else {
                return false;
            }
        },
        placeholderstatue: function () {
            if (this.mapsearchvalue != '' && this.mapsearchvalue != '请输入警员姓名/警号/设备名称') {
                return 1;
            } else {
                return 0;
            }
        }
    },
    emptyinput(event) {
        this.mapsearchvalue = '';
        this.placeholderstatue = 0;
        $("#mapsearch").focus();
    },
    focusinput() {
        if (this.mapsearchvalue != '' && this.placeholderstatue != 0) {
            this.placeholderstatue = 1; //表示显示x,0不显示x
        } else {
            this.emptyinput();
        }
    },
    restoretip(e) {
        e.preventDefault();
        $("#mapsearch").blur();
        if (this.mapsearchvalue == '') {
            this.mapsearchvalue = '请输入警员姓名/警号/设备名称';
            this.placeholderstatue = 0;
        }
    },
    mapsearch() {
        if ($.trim(this.mapsearchvalue) == '' || this.placeholderstatue == 0) {
            return;
        }
        let data = {};
        data = sszhmap.mapsearchvalue;
        ajax({
            url: '/gmvcs/uom/device/dsj/dsjInfo',
            method: 'post',
            data: data
        }).then(result => {
            if (result.code != 0) {
                notification.warn({
                    title: '通知',
                    message: "找不到警员和设备"
                });
                return;
            } else if (result.data.length <= 0) {
                notification.warn({
                    title: '通知',
                    message: "找不到警员和设备"
                });
                return;
            } else {
                var devarr = [];
                for (var i = 0; i < result.data.length; i++) {
                    if (result.data[i].online == 0) {
                        devarr.push(result.data[i].gbcode);
                    }
                }
                if (devarr.length <= 0) {
                    notification.warn({
                        title: '通知',
                        message: "无警员和设备在线"
                    });
                    return;
                }
                let pData = {
                    'devices': devarr,
                    'deviceType': "DSJ"
                };
                ajax({
                    url: '/gmvcs/instruct/mapcommand/devicegps',
                    method: 'post',
                    data: pData
                }).then(result => {
                    if (result.code != 0) {
                        notification.warn({
                            title: '通知',
                            message: "获取设备信息失败"
                        });
                        return;
                    }
                    if (JSON.stringify(result.data) == '{}') {
                        notification.warn({
                            title: '通知',
                            message: "搜索的设备无GPS"
                        });
                        return;
                    }
                    let point = {};
                    avalon.each(result.data, function (index, item) {
                        item.gbcode = item.deviceId;
                        createUpdatemarkerArr(item, false); //不把地图中心定位到这个人
                        point.lon = item.longitude;
                        point.lat = item.latitude;
                    })
                    //initMapObject.setMapCenter(point.lon, point.lat,8);//缩小地图层级，
                    $('#mapIframe')[0].contentWindow.esriMap.setMapCenter(point.lon, point.lat, 8);
                });
            }
        })
    },
    handleQuickSearch(event) {
        if (event.keyCode == 13) {
            this.mapsearch();
        }
    },
    showcityName: "广州",
    citylocate(event) {
        let city = event.target.innerText;
        let lon = avalon.vmodels['sszhxt_vm'].$cityDetailobj.cityobj[city].lon;
        let lat = avalon.vmodels['sszhxt_vm'].$cityDetailobj.cityobj[city].lat;
        $('#mapIframe')[0].contentWindow.esriMap.setMapCenter(lon, lat, 10);
    }
});
//视频播放
let sszhsp = avalon.define({
    $id: 'sszhdtspck',
    name: '',
    word: '声音',
    ifallowtalk: isAllowSpeak,
    $computed: {
        xhword: function () {
            if (sszhsp.signal < 15) {
                return '差';
            } else if (sszhsp.signal > 50) {
                return '良';
            } else {
                return '优';
            }
        }
    },
    signal: 0, //信号强度
    dev: '',
    gbcode: '', //表示窗口的gbcode,执法仪gbcode跟childGbcode一样，多通道时这个是父的gbcode
    childGbcode: '', //正在点流的gbcode,多通道是就是某个通道的gbcode
    deviceName: '', //设备名称
    deviceType: '', //设备类型
    gbcodeArr: [], //通道数组
    isShowTools: false, //是否显示右侧工具
    showToolBtn:false,//是否显示控制按钮
    checkItem: '', //选择的通道
    oldCheckItem: '', //之前选择的通道，用于对比现在点的通道是不是跟之前一样
    isMove: false, //鼠标是否按下
    imgClientx: 0, //鼠标按下的位置
    imgMoveX: 125, //图标移动的距离
    imgStartx: 125, //图标开始的left值
    mytype: '', //设备类型mytype表示自定义设备类型0 执法仪， 1：快速 2：车 3:无人机
    checkIndexFn(e,item) {
        //进行点流操作
        let v = e.target.value;
        if (v == this.oldCheckItem) {
            return;
        }
        this.oldCheckItem = v;
        this.playVideo(v);
        let _this = this;
        avalon.each(sszhmap.devhtmllist, function (index, value) {
          //判断是否显示控制按钮
            if(value.gbcode == item.deviceGbcode){
                if(item.PTZControllable == 1){
                    _this.showToolBtn = true;
                }else{
                    _this.showToolBtn = false;
                }
                v.childGbcode = value.gbcode;
            }
        })
        //sszhinfowindow.playVideo(item.deviceGbcode, '','',this.signal, this.deviceName, this.mytype,this.gbcodeArr);
    },
    moveImg(e) {
        let x;
        if (this.isMove) {
            if (e.clientX - this.imgClientx >= 0) {
                x = this.imgStartx + e.clientX - this.imgClientx;
                if (x >= 240) {
                    this.imgMoveX = 240;
                    return;
                }
                this.imgMoveX = x;
            } else {
                x = this.imgStartx - Math.abs(e.clientX - this.imgClientx);
                if (x <= 0) {
                    this.imgMoveX = 0;
                    return;
                }
                this.imgMoveX = x;
            }
        }
    },
    moveUpImg(e) {
        this.isMove = false;
    },
    moveDownImg(e) {
        this.isMove = true;
        this.imgClientx = e.clientX;
        this.imgStartx = this.imgMoveX;
    },
    showsszhspth: true,
    sszhyycontrol: 1,
    sszhsptalkcontrol: false, //false表示没有语音对讲，true表示有
    sszhsptalkword: '对讲',
    closesszhspth() {
        //this.showsszhspth =false;
        $(".sszhspth").css({
            'z-index': 0
        });
        $('#npGSVideoPlugin').css({
            'z-index': 0
        });
        $('.sszhxt-sszh-map').show();
        if (0 != player.getStatusByIndex(-1)) {
            player.stopRec(1); //结束视频
        }
        if (speakPerson.gbcode != this.childGbcode) { //不能关闭了人家单独点开的语音
            player.stopTalk(this.gbcode); //结束音频
        }
        this.sszhsptalkcontrol = false;
        this.sszhsptalkword = '对讲';
        this.sszhyycontrol == 1;
        //this.word ='开启';
        //删除对应的左侧栏
        avalon.each(sszhmap.devhtmllist, function (index, item) {
            if (item.gbcode == sszhsp.gbcode) {
                clearInterval(item.timeobj); //清除定时器
                sszhmap.devhtmllist.removeAt(index);
                return;
            }
        });
        this.gbcode = '';
        this.sszhspthtime = "00:00:00";
    },
    hidesszhspth() {
        //this.showsszhspth =false;
        $(".sszhspth").css({
            'z-index': 0
        });
        $('#npGSVideoPlugin').css({
            'z-index': 0
        });
        //$('.sszhxt-sszh-map').show();
        //因为公用ocx，最小化的时候其实是关闭流的，点开在重连
        if (0 != player.getStatusByIndex(-1)) {
            player.stopRec(1); //结束视频
        }
        if (speakPerson.gbcode != this.childGbcode) { //不能关闭了人家单独点开的语音
            player.stopTalk(this.gbcode); //结束音频
        }

        this.sszhsptalkcontrol = false;
        this.sszhsptalkword = '对讲';
        this.sszhyycontrol == 1;
        //this.word ='开启';
        //显示对应的左侧栏
        avalon.each(sszhmap.devhtmllist, function (index, item) {
            if (item.gbcode == sszhsp.gbcode) {
                item.show = true;
            }
        });

    },
    sszhspthtime: "00:00:00",
    slient() {
        player.SoundCtrl(1, this.sszhyycontrol, 1);
        if (this.sszhyycontrol === 0) {
            this.sszhyycontrol = 1;
            this.word = '开启';
        } else {
            this.sszhyycontrol = 0;
            this.word = '静音';
        }
        return this.sszhyycontrol;
    },
    starttalk(gbcode, symbol, callback) { //symbol用来判断这个语音是点击视屏的语音还是地图那边调用的语音
        var gb = gbcode;
        if (!symbol) {
            gb = this.gbcode;
        }
        if (!symbol && this.sszhsptalkcontrol) {
            var code = player.stopTalk(this.gbcode); //结束音频
            if (code == 0) {
                this.sszhsptalkcontrol = false;
                this.sszhsptalkword = '对讲';
            } else {
                notification.error({
                    title: '通知',
                    message: '结束语音失败'
                });
            }
            return;
        }
        ajax({
            url: '/gmvcs/uom/ondemand/dsj/intranet/streamserver?requestType=play_realtime_speak&deviceId=' + gb,
            method: 'get',
            data: null,
        }).then(result => {
            if (result.code == 1702) {
                notification.error({
                    title: '通知',
                    message: '设备不在线'
                });
                return;
            } else if (result.code == 1701) {
                notification.error({
                    title: '通知',
                    message: '获取流媒体信息失败'
                });
                return;
            } else if (result.code == 0) {
                result.data.gbcode = gb;
                let code = player.login(result.data); //先登录流媒体
                if (code != 0) {
                    notification.error({
                        title: '通知',
                        message: '流媒体服务登录失败,出错代码为' + code
                    });
                    return;
                }
                code = player.startTalk(result.data); //登录成功，进行语音呼叫
                if (code == -2) { //返回-2表示没有登录成功
                    return;
                } else if (code == -4) { //放回-4表示当前已有对讲，先关闭
                    notification.warn({
                        title: '通知',
                        message: '当前已经有语音对讲，请先关闭'
                    });
                    return;
                }
                if (code != 0) {
                    notification.error({
                        title: '通知',
                        message: '启动对讲失败'
                    });
                    return;
                }
                if (!symbol) {
                    this.sszhsptalkcontrol = true;
                    this.sszhsptalkword = '结束';
                }

                callback && callback(code);
                return code;
            }
        });
    },
    playVideo: function (childGbcode, callback) { //实时点流
        //因为是公用一个ocx播放器，确定ocx一定是空闲状态
        if (0 != player.getStatusByIndex(-1)) {
            player.stopRec(1);
        }
        //$('.sszhxt-sszh-map').hide();
        ajax({
            url: '/gmvcs/uom/ondemand/dsj/intranet/streamserver?requestType=play_realtime_video&deviceId=' + childGbcode,
            method: 'get',
            data: null
        }).then(result => {
            if (result.code == 1702) {
                notification.error({
                    title: '通知',
                    message: '设备不在线'
                });
                return;
            } else if (result.code == 1701) {
                notification.error({
                    title: '通知',
                    message: '获取流媒体信息失败'
                });
                return;
            } else if (result.code == 1500) {
                notification.error({
                    title: '通知',
                    message: '获取流媒体信息失败'
                });
                return;
            } else if (result.code == 0) {
                result.data.gbcode = childGbcode;
                if (result.data.gb28181Mode && result.data.gb28181Mode == 1) { //表示gsp方式点流
                    result.data.url = result.data.gsp;
                    let code = player.playRecByUrl(result.data);
                    if (code != 0) {
                        notification.error({
                            title: '通知',
                            message: '视频呼叫失败'
                        });
                        return;
                    }
                } else {
                    let code = player.login(result.data); //先登录流媒体
                    if (code != 0) {
                        notification.error({
                            title: '通知',
                            message: '流媒体服务登录失败,出错代码为' + code
                        });
                        return;
                    }
                    code = player.playRec(result.data); //实时点流
                    if (code == -2) { //表示登录失败
                        return;
                    }
                }

                //sszhsp.dev = dev;//保存播放的当前设备
                sszhsp.childGbcode = childGbcode;
                //callback && callback(code);
                //return code;
            }
        });

    },
    // stopvideo(){
    //     sszh.clearsszhspthtime();
    //     player.stopRec(-1);
    // },
    printscreen() {
        var obj = player.printOcxWindow(1);
    //     if (obj.code == 0) {
    //         notification.success({
    //             title: '通知',
    //             message: '截图成功,图片保存路径为D:\\CaptureFolder\\' + obj.time + '.jpg'
    //         });
    //         return;
    //     }
    //     notification.error({
    //         title: '通知',
    //         message: '截图失败'
    //     });
    },
    maxView() {
        player.maxView();
    },
    //还原预置点
    ResetPreset() {

    },
    //向左移动
    moveleft() {

    },
    //向上移动
    moveup() {

    },
    //向右动
    moveright() {

    },
    //向下动
    movedown() {

    },
    //焦距变小
    zoom_out() {

    },
    //焦距变大
    zoom_in() {

    },
    //变远
    far_away() {

    },
    //变近
    far_close() {

    },
    //缩小光圈
    reduce_aperture() {

    },
    //放大光圈
    increase_aperture() {

    },
    //自转
    rotation() {

    }

})
//监听图标是否移动
sszhsp.$watch('imgMoveX', (v) => {
    // console.log(v);
})



//语音通话窗口vm
let sszhyyth = avalon.define({
    $id: 'sszhyyth',
    sszhthjy: '',
    $computed: {
        xhword: function () {
            if (sszhyyth.signal < 15) {
                return '差';
            } else if (sszhyyth.signal > 50) {
                return '良';
            } else {
                return '优';
            }
        }
    },
    signal: 0, //信号强度
    sszhyythtime: '00:00:00',
    countsszhythtime: 0, //语音计时
    sszhyycontrol: 0, //禁音控制开关
    countsszhythtimeObject: '', //语音定时器
    countTime: function () {
        let h, m, s;
        this.countsszhythtime = this.countsszhythtime + 1;
        h = parseInt(this.countsszhythtime / 3600);
        m = parseInt(this.countsszhythtime % 3600 / 60);
        s = parseInt(this.countsszhythtime % 3600 % 60);
        if (h < 10) {
            h = '0' + h;
        }
        if (m < 10) {
            m = '0' + m;
        }
        if (s < 10) {
            s = '0' + s;
        }
        this.sszhyythtime = h + ':' + m + ':' + s;
        this.countsszhythtimeObject = setTimeout(sszhyyth.countTime, 1000);
    },
    sszhyytoggle: false,
    sszhyythaction: 'enter',
    hidesszhyythaction: function () {
        $("#sszhyyth").hide(300, function () {
            $(".sszhyythmincontianer").show(100);
        });
    },
    closesszhhyyth: function () {
        clearTimeout(sszhyyth.countsszhythtimeObject); //清除定时器
        this.sszhyythtime = "00:00:00";
        this.countsszhythtime = 0;
        sszhyyth.sszhyytoggle = false;
        sszhmap.sszhyyhtml = [];
        let code = player.stopTalk(speakPerson.gbcode);
        if (code === 0) {
            sszh.sszhyyhtml = '';
            speakPerson = {}; //清空语音对话保存数据
        } else {
            notification.error({
                title: '通知',
                message: "结束语音失败"
            });
        }

    },
    slience: function () {
        sszhsp.slient(speakPerson.gbcode);
    }
});
//录制视频vm
let sszhlzsp = avalon.define({
    $id: 'sszhlzsp',
    dev: '',
    gbcode: '',
    sszhlzsptoggle: false,
    checkedone: true,
    checkedtwo: false,
    checkedthere: false,
    lzsch: '',
    lzscm: '',
    lzscs: '',
    handleChange(e) {
        if (e.target.value == '1') {
            this.checkedone = true;
            this.checkedtwo = false;
            this.checkedthere = false;
        } else if (e.target.value == '2') {
            this.checkedone = false;
            this.checkedtwo = true;
            this.checkedthere = false;
        } else {
            this.checkedone = false;
            this.checkedtwo = false;
            this.checkedthere = true;
        }
    },
    sszhlzspsure() {
        let pData = {};
        if (this.checkedone) {
            pData.time = '15fenzhong';
            pData.dev = this.gbcode;
        } else if (this.checkedtwo) {
            pData.time = '30';
            pData.dev = this.gbcode;
        } else if (this.checkedthere) {
            pData.time = 3600 * this.lzsch + 60 * this.lzscm + this.lzscs;
            pData.dev = this.gbcode;
            if (pData.time === '0') {
                notification.warn({
                    title: '通知',
                    message: "请设置时间"
                });
                return;
            }
        }
        ajax({

        }).then(result => {

        })
    },
    sszhlzspcancel: function () {
        this.sszhlzsptoggle = false;
    }
});
//消息下发
let sbxxxf = avalon.define({
    $id: 'sbxxxf',
    messagecontent: '',
    dev: '',
    gbcode: '',
    showsbxxxf: false,
    sentmessage() {

    },
    closesbxxxf() {
        this.showsbxxxf = false;
    }
});

avalon.filters.filterByState = function (str) {
    if (str == 1) {
        return '未查看';
    } else {
        return '已查看';
    }
}
//告警信息vm
let sszhgjxxManage = avalon.define({
    $id: 'sszhgjxxManage',
    gjxxlist: [],
    gotogj(item) {
        if (item.state == 0) {
            return;
        }
        let obj = {};
        item.state = 0; //变为已处理
        obj.time = item.time;
        obj.sosPerson = item.userName + '(' + item.userCode + ')';
        obj.gbcode = item.gbcode;
        obj.isRealTimeView = true;
        item.userCode ? obj.userCode = item.userCode : '-';
        item.userName ? obj.userName = item.userName : '-';
        obj.sosId = item.sosId;
        obj.isGjgl = false;
        obj.handleStatus = 'WAITING';
        var data = JSON.stringify(obj);
        sessionStorage.setItem('sszhxt-gjglcontrol', data);
        var baseUrl = document.location.href.substring(0, document.location.href.lastIndexOf("/"));
        window.location.href = baseUrl + "/sszhxt-gjglcontrol";
    }
});



//地图信息窗口的vm
let sszhinfowindow = avalon.define({
    $id: "mapinfowindow",
    playVideo: function (gbcode, username, usercode, signal, name, mytype, devArr) { //实时点流
        if (vm.isie && !ocxele.object || !vm.isie && undefined == ocxele.GS_ReplayFunc) {
            vm.tipText = "当前页面需要高新兴视频播放器插件支持";
            vm.showtip = true;
            vm.downloadTipShow = true;
            return;
        }
        //不显示工具
        if (mytype != 0) {
            sszhsp.isShowTools = true;
            sszhsp.gbcodeArr = devArr;
            $(".sszhspth").css({
                'z-index': 9999,
                'width': 1000,
                'height': '500'
            });
        } else {
            sszhsp.isShowTools = false;
            $(".sszhspth").css({
                'z-index': 9999,
                'width': '700',
                'height': '500'
            });
        }
        $('#npGSVideoPlugin').css({
            'z-index': 999
        });
        if (sszhsp.gbcode == gbcode && 0 != player.getStatusByIndex(-1)) {
            //表示当前视频窗口是同一个设备，多通道也是一样，并且在播放了
            avalon.each(sszhmap.devhtmllist, function (index, item) {
                if (item.gbcode == sszhsp.gbcode) {
                    item.show = false;
                }
            })
            sszhsp.showsszhspth = true;
            return;
        }
        if (sszhsp.gbcode == gbcode && 0 == player.getStatusByIndex(-1)) {
            //表示当前视频窗口是同一个人，只需要重新点流就好,出现场景是从左侧栏点击定位到地图，并且窗口空闲，然后又在地图点击视频呼叫
            //执法仪
            if (mytype == 0) {
                sszhsp.playVideo(gbcode);
                sszhsp.showToolBtn = false;
            } else {
                //多通道的,默认点开第一个通道
                sszhsp.playVideo(sszhsp.childGbcode);
            }

            avalon.each(sszhmap.devhtmllist, function (index, item) {
                if (item.gbcode == sszhsp.gbcode) {
                    item.show = false;
                }
            })
            sszhsp.showsszhspth = true;
            return;
        }

        if (sszhsp.showsszhspth) {
            if(0 != player.getStatusByIndex(-1)){
                //表示ocx已经被占用了,并且是其他人要占用视频播放窗口了，先关闭视频点流，将视频点流的人显示在左侧，在将当前设备点流，播放在ocx
                sszhsp.hidesszhspth();
            }else{
                //ocx空就删掉对应的
                for(var i=0; i<sszhmap.devhtmllist.length;i++){
                    let item = sszhmap.devhtmllist[i];
                    if(sszhsp.gbcode = item.gbcode){
                        sszhmap.devhtmllist.splice(i,1);
                        break;
                    }
                }
            }
        }
        //在左侧栏中找到了这个人，说明他在视频中，并且这个窗口播放过其他人
        let flag = false;
        avalon.each(sszhmap.devhtmllist, function (index, item) {
            if (item.gbcode == gbcode) {
                flag = true;
                item.show = false;
                sszhsp.playVideo(item.childGbcode); //因为点流放回太慢，不用回调了
                sszhsp.gbcode = item.gbcode;
                sszhsp.signal = item.signal;
                sszhsp.mytype = item.mytype;
                sszhsp.showsszhspth = true;
                sszhsp.showToolBtn = item.showToolBtn;
                //多通道只显示设备名称
                if (mytype != 0) {
                    sszhsp.name = item.name;
                } else {
                    if (!username) {
                        sszhsp.name = "与" + name + "视频通话中";
                    } else {
                        sszhsp.name = "与" + username + '(' + usercode + ')' + "视频通话中";
                    }

                }
                sszhsp.deviceName = item.name;
                if (mytype == 1) {
                    sszhsp.deviceType = '快速布控设备';
                } else if (mytype == 2) {
                    sszhsp.deviceType = "车载移动执法设备";
                } else {
                    sszhsp.deviceType = "警用无人机";
                }
                return; //这里只会return回去这个function
            }

        })

        // let code = sszhsp.playVideo(gbcode,dev,function (code) {
        if (mytype == 0) {
            //说明这个人没有被视频，
            sszhsp.playVideo(gbcode); //因为点流放回太慢，不用回调了
        } else {
            sszhsp.playVideo(devArr[0].gbcode); //因为点流放回太慢，不用回调了
        }

        //if(code!=0)return;
        sszhsp.showsszhspth = true;
        //Firefox中ocx不能隐藏，初始设置他的宽高是1px
        // $(".sszhspth").css({
        //     'width': 700,
        //     'height': 500,
        //     'z-index': 9999
        // });
        sszhsp.gbcode = gbcode;
        sszhsp.signal = signal;
        sszhsp.mytype = mytype;
        if (mytype != 0) {
            sszhsp.isShowTools = true;
            sszhsp.gbcodeArr = devArr;
            $(".sszhspth").css({
                'z-index': 9999,
                'width': 1000,
                'height': '500'
            });
        } else {
            sszhsp.isShowTools = false;
            $(".sszhspth").css({
                'z-index': 9999,
                'width': '700',
                'height': '500'
            });
        }
        $('#npGSVideoPlugin').css({
            'z-index': 999
        });
        if (flag) {
            return;
        }
        //视频计时
        let obj = {};
        obj.gbcode = gbcode;
        if (mytype == 0) {
            obj.childGbcode = gbcode;
        } else {
            obj.childGbcode = devArr[0].gbcode;
            if(devArr[0].PTZControllable == 1){
                sszhsp.showToolBtn = true;
            }else{
                sszhsp.showToolBtn = false;
            }
            sszhsp.checkItem = devArr[0].gbcode;
        }
        obj.devArr = devArr;
        obj.username = username;
        obj.usercode = usercode;
        obj.show = false; //左侧栏的显示
        obj.oldTime = new Date(); //视频计时
        obj.time = '00:00:00';
        obj.signal = signal;
        obj.mytype = mytype;
        obj.name = name;
        //let timeobj = setInterval("countspTime('"+dev+"')", 10000);
        let timeobj = setInterval(function () {
            countspTime(sszhsp.gbcode);
        }, 1000);
        obj.timeobj = timeobj;
        if (mytype != 0) {
            sszhsp.name = name;
            sszhsp.deviceName = name;
            obj.showName = "与" + name + "视频通话中";
            if (mytype == 1) {
                sszhsp.deviceType = '快速布控设备';
            } else if (mytype == 2) {
                sszhsp.deviceType = "车载移动执法设备";
            } else {
                sszhsp.deviceType = "警用无人机";
            }

        } else {
            if (!username) {
                sszhsp.name = "与" + name + "视频通话中";
                obj.showName = "与" + name + "视频通话中";
            } else {
                sszhsp.name = "与" + username + '(' + usercode + ')' + "视频通话中";
                obj.showName = "与" + username + '(' + usercode + ')' + "视频通话中";
            }
        }
        sszhmap.devhtmllist.push(obj);
        //});



    },
    startTalk: function (gbcode, username, usercode, signal, mytype) { //语音对讲
        if (vm.isie && !ocxele.object || !vm.isie && undefined == ocxele.GS_ReplayFunc) {
            vm.tipText = "当前页面需要高新兴视频播放器插件支持";
            vm.showtip = true;
            vm.downloadTipShow = true;
            return;
        }
        if (speakPerson.gbcode === gbcode) {
            notification.info({
                title: '通知',
                message: '该警员已经在语音通话中'
            });
            return;
        }
        sszhsp.starttalk(gbcode, true, function (code) {
            if (code != 0) {
                notification.error({
                    title: '通知',
                    message: '启动对讲失败'
                });
                return;
            };
            let obj = {};
            if(!username){
                username = '-';
            }
            if(!usercode){
                usercode = '-';
            }
            obj.gbcode = gbcode;
            obj.username = username;
            obj.usercode = usercode;
            obj.signal = signal;
            obj.mytype = mytype;
            obj.showName =  "与" + username + '(' + usercode + ')' + "语音通话中";
            sszhyyth.sszhyytoggle = true;
            sszhyyth.countTime(); //语音计时
            speakPerson.gbcode = gbcode;
            sszhyyth.sszhthjy = "与" + username + '(' + usercode + ')' + "语音通话中";
            sszhmap.sszhyyhtml.push(obj);
        });



    },
    startRecord: function (gbcode) { //视频录制
        sszhlzsp.sszhlzsptoggle = true;
        sszhlzsp.gbcode = gbcode;
    },
    sentmessage: function (gbcode) { //消息下发
        sbxxxf.showsbxxxf = true;
        sbxxxf.gbcode = gbcode;
    },
    lock: function (gbcode, lock, callback) { //远程锁定
        let data = {};
        let optCmd;
        if (lock) {
            optCmd = 'Lock';
        } else {
            optCmd = 'Unlock';
        }
        ajax({
            url: '/gmvcs/uom/device/dsj/control/lock?deviceId=' + gbcode + '&optCmd=' + optCmd,
            method: 'post',
            data: null
        }).then(result => {
            if (result.code != 0) {
                notification.error({
                    title: '通知',
                    message: '服务器异常,操作失败'
                });
                $('#mapIframe')[0].contentWindow.addOrRemoveMask(false);
                return;
            }
            if (lock) {
                if (mapType == 0) {
                    $('#mapIframe')[0].contentWindow.addOrRemoveMask(true, '正在锁定', true);
                    setTimeout(function () {
                        $('#mapIframe')[0].contentWindow.addOrRemoveMask(false);
                        $('#mapIframe')[0].contentWindow.addOrRemoveMask(true, '已锁定', false);
                        $('#mapIframe')[0].contentWindow.disableOrActiveButton(true);
                        $('#mapIframe')[0].contentWindow.settleData(gbcode, false, true);
                        changeTreeImg(gbcode, true);
                    }, 500);
                } else {
                    notification.success({
                        title: '通知',
                        message: '锁定成功'
                    });
                    $('#mapIframe')[0].contentWindow.disableOrActiveButton(true);
                    $('#mapIframe')[0].contentWindow.settleData(gbcode, false, true);
                    changeTreeImg(gbcode, true);
                }


            } else {
                if (mapType == 0) {
                    $('#mapIframe')[0].contentWindow.addOrRemoveMask(true, '正在解锁', true);
                    setTimeout(function () {
                        $('#mapIframe')[0].contentWindow.addOrRemoveMask(false);
                        $('#mapIframe')[0].contentWindow.addOrRemoveMask(true, '已解锁', false);
                        $('#mapIframe')[0].contentWindow.disableOrActiveButton(false);
                        $('#mapIframe')[0].contentWindow.settleData(gbcode, false, false);
                        changeTreeImg(gbcode, false);
                    }, 500);
                } else {
                    notification.success({
                        title: '通知',
                        message: '解锁成功'
                    });
                    $('#mapIframe')[0].contentWindow.disableOrActiveButton(false);
                    $('#mapIframe')[0].contentWindow.settleData(gbcode, false, false);
                    changeTreeImg(gbcode, false);
                }


            }
            if (mapType == 0) {
                setTimeout(function () {
                    $('#mapIframe')[0].contentWindow.addOrRemoveMask(false);
                }, 1000);
            }

            callback && callback();
        });

    },
    photograph: function (gbcode) { //远程拍照
        let data = {};
        ajax({
            url: '/gmvcs/uom/device/dsj/control/photo?deviceId=' + gbcode,
            method: 'post',
            data: null
        }).then(result => {
            if (result.code != 0) {
                notification.error({
                    title: '通知',
                    message: '拍照指令下发失败'
                });
                return;
            }
            if (mapType == 0) {
                $('#mapIframe')[0].contentWindow.addOrRemoveMask(true, '拍照指令已下发', false);
                setTimeout(function () {
                    $('#mapIframe')[0].contentWindow.addOrRemoveMask(false);
                }, 1000)
            } else {
                notification.success({
                    title: '通知',
                    message: '拍照指令已下发'
                });
            }
        });
    },
    record: function (gbcode, record) {
        let data = {};
        let optCmd;
        if (record) {
            optCmd = 'Start';
        } else {
            optCmd = 'Stop';
        }

        ajax({
            url: '/gmvcs/uom/device/dsj/control/record?deviceId=' + gbcode + '&optCmd=' + optCmd,
            method: 'post',
            data: null
        }).then(result => {
            if (result.code != 0) {
                notification.error({
                    title: '通知',
                    message: '服务器异常,操作失败'
                });
                return;
            }
            if (record) {
                if (mapType == 0) {
                    $('#mapIframe')[0].contentWindow.addOrRemoveMask(false);
                    $('#mapIframe')[0].contentWindow.changeSpanCss(false);
                    $('#mapIframe')[0].contentWindow.settleData(gbcode, true, false);
                    $('#mapIframe')[0].contentWindow.addOrRemoveMask(true, '录像开始指令下发成功', false);
                } else {
                    $('#mapIframe')[0].contentWindow.changeSpanCss(false);
                    $('#mapIframe')[0].contentWindow.settleData(gbcode, true, false);
                    notification.success({
                        title: '通知',
                        message: '录像开始指令下发成功'
                    });
                }

            } else {
                if (mapType == 0) {
                    $('#mapIframe')[0].contentWindow.addOrRemoveMask(false);
                    $('#mapIframe')[0].contentWindow.changeSpanCss(true);
                    $('#mapIframe')[0].contentWindow.settleData(gbcode, false, false);
                    $('#mapIframe')[0].contentWindow.addOrRemoveMask(true, '录像停止指令下发成功', false);
                } else {
                    $('#mapIframe')[0].contentWindow.changeSpanCss(true);
                    $('#mapIframe')[0].contentWindow.settleData(gbcode, false, false);
                    notification.success({
                        title: '通知',
                        message: '录像停止指令下发成功'
                    });
                }


            }
            if (mapType == 0) {
                setTimeout(function () {
                    $('#mapIframe')[0].contentWindow.addOrRemoveMask(false);
                }, 1000)
            }
        });

    },
    controlMarkerArr: controlMarkerArr
})
//视频计时函数
function countspTime(gbcode) {
    avalon.each(sszhmap.devhtmllist, function (index, item) {
        if (item.gbcode == gbcode) {
            let h, m, s;
            let nowTime = new Date();
            let timecount = (nowTime - item.oldTime) / 1000;
            h = parseInt(timecount / 3600);
            m = parseInt(timecount % 3600 / 60);
            s = parseInt(timecount % 3600 % 60);
            if (h < 10) {
                h = '0' + h;
            }
            if (m < 10) {
                m = '0' + m;
            }
            if (s < 10) {
                s = '0' + s;
            }
            item.time = h + ':' + m + ':' + s;
        }
        if (item.gbcode === sszhsp.gbcode) {
            sszhsp.sszhspthtime = item.time; //将时间赋给视频窗口时间
            return;
        }
    });
}


//===============================ocx初始化部分========================================
function initOcx() {
    player = new Gxxplayer();
    ocxele = document.getElementById("npGSVideoPlugin");

    if (vm.isie && !ocxele.object || !vm.isie && undefined == ocxele.GS_ReplayFunc) {
        vm.tipText = "当前页面需要高新兴视频播放器插件支持";
        vm.showtip = true;
        vm.downloadTipShow = true;
        return;
    }
    // 初始化播放器
    player.init({
        'element': 'npGSVideoPlugin',
        'model': 1,
        'proxy': _onOcxEventProxy
    });
    let version = player.getVersion();
    if (compareString(gxxOcxVersion, version)) {
        vm.tipText = '您的高新兴视频播放器插件版本为'+ version +'，最新版为' + gxxOcxVersion + '，请下载最新版本';
        vm.showtip = false;
        vm.downloadTipShow = true;
        return;
    }
    // 分屏为1列4行
    //player.splitWnd(4, 4, 1);
}
function compareString (str1, str2) {
    let num1 = [],num2 =[];
    num1 = str1.split('.');
    num2 = str2.split('.');
    let maxLength = num1.length>num2.length? num1.length : num2.length;
    for(var i =0;i<maxLength;i++){
        if(num1[i] === undefined){
            return false;
        }
        if(num2[i] === undefined){
            return true;
        }
        if(Number(num1[i]) > Number(num2[i])){
            return true;
        }else if(Number(num1[i]) < Number(num2[i])){
            return false;
        }else if(Number(num1[i]) == Number(num2[i])){
            continue;
        }
    }
    return false;
}
function _onOcxEventProxy(data) {
    let ret = eval('(' + data + ')'); //每次操作acx都会回调这里，如点击关闭窗口回调此处，放回值如下
    //表示截图
    if(ret.action == "CapturePicture"){
        if(ret.code == 0){
            notification.success({
                title: '通知',
                message: '截图成功，保存路径为' + ret.data.picPath
            });
        }else{
            notification.error({
                title: '通知',
                message: '截图失败，错误代码' + ret.code
            });
        }
    }else if(ret.action == "StartLocalRecord"){
        if(ret.code == 0){
            notification.success({
                title: '通知',
                message: '本地录像成功，保存路径为' + ret.data.szLocalRecordPath
            });
        }else{
            notification.error({
                title: '通知',
                message: '本地录像失败，错误代码' + ret.code
            });
        }
    }
    // //{"action":"StopVideo","code":0,"data":{"nIndex":1,"szNodeID":"35000...1668//设备编号"},"ocxID":"realtime_player"}
    // if(ret.action == "SelDisp"){
    //     if(ret.data.szNodeID == undefined || ret.data.szNodeID == null||ret.data.szNodeID ===""){
    //         return;
    //     }
    //     searchNode(ret.data.szNodeID);//点击ocx,部门树定位警员
    // }
}
// //=====================================sos告警===========================
// var error_total = 0;
// const websocketIP = 'ws://'+ webSocketIp;
// const socket = io(websocketIP, {
//     transports: ['polling']
// });
//
// socket.on('connect', (connect) => {
//     //获取当前登录用户的部门path
//     ajax({
//         url :'/gmvcs/uap/cas/loginUser',
//         method : 'get',
//         data : null
//     }).then( result => {
//         if (result.code != 0) {
//             notification.warn({
//                 title: '通知',
//                 message: "获取用户部门信息失败"
//             });
//             return;
//         }
//         longinPersonOrgpath = result.data.orgPath;
//         longinPersonUid = result.data.uid;
//         socket.emit('uidEvent', {
//             'uid': longinPersonUid
//         });
//     });
//
//    //console.log(socket);
//
// });
//
// // socket.on('CAN', (ret) => {
// //     console.log("CAN:", ret);
// //     //      setTimeout(function() {
// //     socket.emit('TOKEN', {
// //         'token': storage.getItem('token')
// //     });
// //     //      }, 500);
// //
// // });
//
// socket.on('messageevent', (ret) => {
//     if( !new RegExp(longinPersonOrgpath).test(ret.orgPath)){
//         return;//不在管辖部门，不显示
//     }
//     let pData = {
//         'devices' : [ret.deviceId],
//         'deviceType': "DSJ"
//     };
//     ajax({
//         url: '/gmvcs/instruct/mapcommand/devicegps',
//         method: 'post',
//         data: pData
//     }).then(result => {
//         let obj = {};
//         if (result.code != 0) {
//             obj.longitude = '经度:-';
//             obj.latitude = '纬度:-';
//         }
//         if(!result.data[ret.deviceId]){
//             obj.longitude = '经度:-';
//             obj.latitude = '纬度:-';
//         }else{
//             obj.longitude = '经度:'+result.data[ret.deviceId].longitude;
//             obj.latitude = '纬度:'+result.data[ret.deviceId].latitude;
//         }
//         if(ret.sosType =='SOS'){
//             obj.type ="SOS告警";
//         }
//         sszhmap.toggleshow();
//         obj.person = ret.userName + '记录仪';
//         obj.gbcode = ret.deviceId;
//         obj.userName = ret.userName;
//         obj.time = ret.sosDate;
//         obj.state = 1;
//         obj.sosId = ret.sosId;
//         // obj.longitude = ret.longitude;
//         // obj.latitude = ret.latitude;
//         obj.userCode = ret.userCode;
//         sszhgjxxManage.gjxxlist.push(obj);
//     })
//
//
// });
//
// socket.on('STATUS', (ret) => {
//     // if(ret.closed == "true") {
//     //     socket.close();
//     // }
// });
//
// socket.on('error', (error) => {
//     socket.close();
// });
// socket.on('reconnecting', (error) => {
//    //console.log(error);
// });
// socket.on('connect_error', (error) => {
//     error_total++;
//     if(error_total > 9) {
//         error_total = 0;
//         socket.close();
//        // console.log("=====重连次数超出限制，已断开socket连接=====");
//     }
// });
//
// socket.on('connect_timeout', (timeout) => {
//    // console.log("connect_timeout:", timeout);
// });
//===============================自定义拖动=============================
let dargObject = {
    a: '',
    b: '',
    c: '',
    w: '',
    h: '',
    move: function (o, e, w, h) {
        this.a = o;
        this.w = w;
        this.h = h;
        document.all ? this.a.setCapture() : window.captureEvents(Event.MOUSEMOVE);
        this.b = e.clientX - parseInt(this.a.style.left);
        this.c = e.clientY - parseInt(this.a.style.top);
        o.style.zIndex = this.getMaxIndex() + 1;
    },
    getMaxIndex: function () {
        let index = 0;
        let sszhyyth = document.getElementById('sszhyyth');
        let sszhlzsp = document.getElementById('sszhlzsp');
        if (sszhyyth.style.zIndex > sszhlzsp.style.zIndex) {
            index = sszhyyth.style.zIndex
        } else {
            index = sszhlzsp.style.zIndex
        }
        return parseInt(index);
    }
};
document.onmouseup = function () {
    if (!dargObject.a) return;
    document.all ? dargObject.a.releaseCapture() : window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
    dargObject.a = "";
};
document.onmousemove = function (d) {
    if (!dargObject.a) return;
    if (!d) d = event;
    if (d.clientX - dargObject.b < 0) { //控制左边界消失
        dargObject.a.style.left = '0px';
    } else if (d.clientX - dargObject.b + dargObject.w > $('.sszh-map').width()) { //控制右边界消失
        dargObject.a.style.left = ($('.sszh-map').width() - dargObject.w) + "px";
    } else {
        dargObject.a.style.left = (d.clientX - dargObject.b) + "px";
    }
    if (d.clientY - dargObject.c < 0) { //控制上边界消失
        dargObject.a.style.top = '0px';
    } else if (d.clientY - dargObject.c + dargObject.h > $('.sszh-map').height()) {
        dargObject.a.style.top = ($('.sszh-map').height() - dargObject.h) + "px"; //控制下边界消失
    } else {
        dargObject.a.style.top = (d.clientY - dargObject.c) + "px";
    }
};