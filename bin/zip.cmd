@echo off

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

setlocal
cd /d %~dp0
set path=%path%;%cd%;
set "PackDir=%~dp0..\build\"
set "ToDir=%~dp0..\"

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


set "zipName=%date:~0,4%%date:~5,2%%date:~8,2%%time:~0,2%%time:~3,2%%time:~6,2%"
echo v%zipName% > "%PackDir%\version.inf"

7z a -mmt -tzip "%ToDir%faaj_%1_v%zipName%.zip" "%PackDir%\*" >nul

exit
