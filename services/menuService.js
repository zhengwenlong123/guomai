import avalon from 'avalon2';
import ajax from './ajaxService';
import 'es6-promise/dist/es6-promise.auto';
import {
    notification
} from 'ane';
let storage = require('./storageService').ret;

let uid = storage.getItem('uid');

const menu = [{
        key: 'tyyhrzpt-xtpzgl-yhgl',
        title: '用户管理',
        uri: '/tyyhrzpt-xtpzgl-yhgl',
        lic: "CAS_MENU_YHGL"
        // icon: 'glyphicon userManager-icon'
    },
    {
        key: 'tyyhrzpt-xtpzgl-bmgl',
        title: '部门管理',
        uri: '/tyyhrzpt-xtpzgl-bmgl',
        lic: "CAS_MENU_BMGL"
        // icon: 'glyphicon orgManager-icon'
    },
    {
        key: 'tyyhrzpt-xtpzgl-gnqx',
        title: '角色管理',
        uri: '/tyyhrzpt-xtpzgl-gnqx',
        lic: "CAS_MENU_GNQX"
        // icon: 'glyphicon funcRoleManager-icon'
    },
    // {
    //     key: 'tyyhrzpt-xtpzgl-xtcd',
    //     title:'系统菜单',
    //     uri: '/tyyhrzpt-xtpzgl-xtcd',
    //     // icon: 'glyphicon sysMenuManager-icon'
    // },
    // {
    //     key: 'tyyhrzpt-xtpzgl-xtgg',
    //     title:'系统公告',
    //     uri: '/tyyhrzpt-xtpzgl-xtgg',
    //     // icon: 'glyphicon sysNoticeManager-icon'
    // },
    {
        key: 'tyyhrzpt-xtpzgl-sjzd',
        title: '数据字典',
        uri: '/tyyhrzpt-xtpzgl-sjzd',
        lic: "CAS_MENU_SJZD"
        // icon: 'glyphicon dictManager-icon'
    },
    {
        key: 'tyyhrzpt-xtpzgl-czrz',
        title: '操作日志',
        uri: '/tyyhrzpt-xtpzgl-czrz',
        lic: "CAS_MENU_CZRZ"
        // icon: 'glyphicon Worklog-icon'
    }
    /*,
        {
            key:'aqsj-index',
            title:'安全审计',
            uri: '/aqsj-index'
        },
        {
            key:'sbgl-index',
            title:'设备管理',
            uri: '/sbgl-index'
        },
        {
            key:'ywgl-index',
            title:'运维管理',
            uri: '/ywgl-index'
        },
        {
            key:'xtgl-index',
            title:'系统管理',
            uri: '/xtgl-index'
        }*/
];


/*
 *二级菜单
 *parentKey：对应一级菜单的key
 *child：二级菜单数组
 */
const menubar = [{
        parentKey: 'aqsj-index',
        child: [{
                key: 'glxtrz',
                title: '管理系统日志',
                uri: '/aqsj-glxtrz'
            },
            {
                key: 'sjcjjccsbrz',
                title: '数据采集及存储设备日志',
                uri: '/aqsj-sjcjjccsbrz'
            },
            {
                key: 'zfjlyrz',
                title: '执法记录仪日志',
                uri: '/aqsj-zfjlyrz'
            }
        ]
    },
    {
        parentKey: 'ywgl-index',
        child: [{
                key: 'sjcjjccsbgl',
                title: '数据采集及存储设备管理',
                uri: '/ywgl-sjcjjccsbgl'
            },
            {
                key: 'yccfwqgl',
                title: '云存储服务器管理',
                uri: '/ywgl-yccfwqgl'
            },
            {
                key: 'yyfwqgl',
                title: '应用服务器管理',
                uri: '/ywgl-yyfwqgl'
            },
            {
                key: 'sjkfwqgl',
                title: '数据库服务器管理',
                uri: '/ywgl-sjkfwqgl'
            },
            {
                key: 'sqgl',
                title: '授权管理',
                uri: '/ywgl-sqgl'
            }
        ]
    },
    {
        parentKey: 'xtgl-index',
        child: [{
                key: 'dwgl',
                title: '单位管理',
                uri: '/xtgl-dwgl'
            },
            {
                key: 'yhgl',
                title: '用户管理',
                uri: '/xtgl-yhgl'
            },
            {
                key: 'qxsz',
                title: '权限设置',
                uri: '/xtgl-qxsz'
            }
        ]
    }
];

let application_menu = [{ //管理菜单列表
        url: "/sszhxt.html#!/sszhxt-sszh", //路由地址
        icon: "icon-sszh", //图标
        bg_color: "#dc6141", //背景颜色
        title: "实时指挥系统", //菜单名称
        lic: "/gmvcs/instruct" //授权校验字段
    },
    {
        url: "/tyyhrzpt.html",
        icon: "icon-xtpzgl",
        bg_color: "#2db7f5",
        title: "系统配置管理",
        lic: "/gmvcs/uap"
    },
    {
        url: "/tyywglpt.html#!/tyywglpt-sbzygl-zfygl",
        icon: "icon-tyywgl",
        bg_color: "#e09e1f",
        title: "统一运维管理",
        lic: "/gmvcs/uom"
    },
    {
        url: "/zfsypsjglpt.html",
        icon: "icon-yspsjgl",
        bg_color: "#20b362",
        title: "视音频数据管理",
        lic: "/gmvcs/audio"
    }
];

// 实时指挥系统模拟数据
// let sszh_menu = {
//     "/gmvcs/sszhxt": [
//         "SSZH_MENU_SSZH",
//         "SSZH_MENU_SPJK",
//         "SSZH_MENU_LXHF",
//         "SSZH_MENU_GJHF",
//         "SSZH_MENU_GJGL",
//         //    "SSZH_MENU_DZWL",
//         //  "SSZH_MENU_JQDJ",
//         "SSZH_MENU_XXCJ",
//         "SSZH_FUNC_SSZH_CHECK",
//         "SSZH_FUNC_SSZH_DEL",
//         "SSZH_FUNC_GJGL_DEL",
//         "SSZH_FUNC_GJGL_ADD",
//         "SSZH_FUNC_GJGL_CHECK",
//         "SSZH_FUNC_DZWL_EDIT"
//     ]
// };

// 根据权限过滤菜单
const menuPromise = new Promise((rs, rj) => {
    // ====================================================================================
    //  ajax({
    //      url: '/api/loged',
    //      type: 'get'
    //  }).then((result) => {
    //      if(result.code === '0') {
    //          $('#loadImg').css('display', 'none');
    //          $('.login-area').removeClass('hidden').addClass('animated flipInX');
    //          travelMenu(menu, result.data.t.functions, result.data.t.allowedFunctions);
    //          avalon.mix(avalon.vmodels.root, {
    //              user: result.data.t
    //          });
    //          rs(menu.slice(0));
    //      } else {
    //          rj(result);
    //      }
    //  });
    // ===================================================================================
    // travelMenu(menu, {}, {});
    // avalon.mix(avalon.vmodels.root, {
    // 	user: {}
    // });
    // rs(menu.slice(0));
    
    if (!storage.getItem('license')) {
        ajax({
            // url: '/api/menu',
            url: '/gmvcs/uap/roles/getUserPrivilege?uid=' + uid,
            method: 'get',
            data: {}
        }).then((result) => {
            if (0 != result.code) {
                notification.error({
                    message: '服务器后端异常，请联系管理员。',
                    title: '温馨提示'
                });
                // setTimeout(() => {
                //     global.location.href = '/main-login.html';
                // }, 2000);
                // return;
            }

            let MENU = {}; //存储已授权菜单对象
            let APP_MENU = []; //授权平台菜单数组 -- 一级菜单
            let CAS_MENU_TYYHRZPT = []; //统一用户认证平台菜单数组     -- 二级菜单
            let CAS_FUNC_TYYHRZPT = []; //统一用户认证平台各模块按钮权限数组

            let UOM_MENU_TYYWGLPT_ARR = []; //统一运维管理平台菜单及功能数组 -- 原数据
            let UOM_MENU_TYYWGLPT = []; //统一运维管理平台菜单及功能数组
            let AUDIO_MENU_SYPSJGL_ARR = []; //视音频数据管理平台菜单及功能数组 -- 原数据
            let AUDIO_MENU_SYPSJGL = []; //视音频数据管理平台菜单及功能数组
            let SSZH_MENU_SSZHXT_ARR = []; //实时指挥系统平台菜单及功能权限数组 -- 原数据
            let SSZH_MENU_SSZHXT = []; //实时指挥系统平台菜单及功能权限数组

            let menuList = [];
            let res = result.data;

            for (let i in res) {
                let item = {};
                item[i] = res[i];
                menuList.push(item);
            }

            // 加入实时指挥系统模拟数据
            // menuList.push(sszh_menu);

            avalon.each(menuList, function (index, el) {
                for (let i in el) {
                    if (el.hasOwnProperty(i)) {
                        avalon.each(application_menu, function (idx, ele) {
                            if (ele.lic == i) {
                                switch (ele.lic) {
                                    // 统一用户认证平台
                                    case "/gmvcs/uap":
                                        getCasMenu(el[i], menu, CAS_MENU_TYYHRZPT, CAS_FUNC_TYYHRZPT);
                                        break;
                                        // 统一运维管理平台
                                    case "/gmvcs/uom":
                                        for (let key in el[i]) {
                                            UOM_MENU_TYYWGLPT.push(key);
                                        }
                                        UOM_MENU_TYYWGLPT_ARR = el[i];
                                        // avalon.Array.merge(UOM_MENU_TYYWGLPT, el[i]);
                                        break;
                                        // 视音频数据管理平台
                                    case "/gmvcs/audio":
                                        for (let key in el[i]) {
                                            AUDIO_MENU_SYPSJGL.push(key);
                                        }
                                        AUDIO_MENU_SYPSJGL_ARR = el[i];
                                        // avalon.Array.merge(AUDIO_MENU_SYPSJGL, el[i]);
                                        break;
                                    case "/gmvcs/instruct":
                                        for (let key in el[i]) {
                                            SSZH_MENU_SSZHXT.push(key);
                                        }
                                        SSZH_MENU_SSZHXT_ARR = el[i];
                                        // el[i].splice(9,1);//隐藏电子围栏菜单
                                        // el[i].splice(19,1);//隐藏集群对讲菜单
                                        // avalon.Array.merge(SSZH_MENU_SSZHXT, el[i]);
                                }
                                APP_MENU.push(ele);
                                return;
                            }
                        });
                    }
                }
            });

            MENU.APP_MENU = APP_MENU; //APP_MENU 已授权的平台菜单
            MENU.CAS_MENU_TYYHRZPT = CAS_MENU_TYYHRZPT; //CAS_MENU_TYYHRZPT 统一用户认证平台菜单
            MENU.CAS_FUNC_TYYHRZPT = CAS_FUNC_TYYHRZPT; //CAS_FUNC_TYYHRZPT 统一用户认证平台各模块按钮权限数组

            MENU.UOM_MENU_TYYWGLPT_ARR = UOM_MENU_TYYWGLPT_ARR; //UOM_MENU_TYYWGLPT 统一运维管理平台菜单及功能权限数组 -- 原数据
            MENU.UOM_MENU_TYYWGLPT = UOM_MENU_TYYWGLPT; //UOM_MENU_TYYWGLPT 统一运维管理平台菜单及功能权限数组
            MENU.AUDIO_MENU_SYPSJGL_ARR = AUDIO_MENU_SYPSJGL_ARR; //AUDIO_MENU_SYPSJGL 视音频数据管理平台菜单及功能权限数组 -- 原数据
            MENU.AUDIO_MENU_SYPSJGL = AUDIO_MENU_SYPSJGL; //AUDIO_MENU_SYPSJGL 视音频数据管理平台菜单及功能权限数组
            MENU.SSZH_MENU_SSZHXT_ARR = SSZH_MENU_SSZHXT_ARR; //SSZH_MENU_SSZHXT 实时指挥系统平台菜单及功能权限数组 -- 原数据
            MENU.SSZH_MENU_SSZHXT = SSZH_MENU_SSZHXT; //SSZH_MENU_SSZHXT 实时指挥系统平台菜单及功能权限数组

            rs(MENU);
        });
    } else {
        let MENU = {}; //存储已授权菜单对象 UOM_MENU_TYYWGLPT
        MENU.UOM_MENU_TYYWGLPT = ['UOM_MENU_SQGL'];
        rs(MENU);
    }
});

// 获取已授权的菜单，用于动态改变系统平台的title
const sysMenu = new Promise((rs, rj) => {
    let menu = {};
    let uid = storage.getItem('uid');
    // 获取当前登录用户的快捷菜单
    ajax({
        url: '/gmvcs/uap/shortcut/getByUid?uid=' + uid,
        method: 'get',
        data: {}
    }).then(result => {
        if (0 !== result.code) {
            notification.warn({
                message: '服务器后端异常，请联系管理员。',
                title: '温馨提示'
            });
            return;
        }

        // let bsList = [];
        let sysList = [];
        avalon.each(result.data, function (k, v) {
            if ('APPLICATION' == v.model) {
                // bsList.push(v);
            } else {
                sysList.push(v);
            }
        });

        // menu.bsList = bsList; // 已授权应用菜单
        menu.sysList = sysList; // 已授权系统菜单
        rs(menu);
    });
});


/** remote_menu：请求获取的菜单数组， native_menu：本地创建的菜单（数组）， output_cas_menu：遍历处理后的二级菜单（数组），output_fun_menu：模块按钮的功能菜单（数组）,可不传 **/
function getCasMenu(cas_menu, native_menu, output_cas_menu, output_fun_menu) {
    let remote_menu = [];
    let list = [];
    for (let key in cas_menu) {
        remote_menu.push(key);
    }
    avalon.each(native_menu, function (k, v) {
        avalon.each(remote_menu, function (kk, vv) {
            if (v.lic == vv) {
                if (cas_menu[v.lic])
                    v.title = cas_menu[v.lic];
                avalon.Array.ensure(output_cas_menu, v);
                avalon.Array.ensure(list, vv);
                return;
            }
        });
    });
    if ("undefined" != typeof output_fun_menu) {
        avalon.each(list, function (key, val) {
            avalon.Array.remove(remote_menu, val);
        });
        avalon.Array.merge(output_fun_menu, remote_menu);
    }
}

function travelMenu(menulet, functions, allowedFunctions) {
    if (!menulet) {
        return;
    }
    for (let i = 0, item; item = menulet[i++];) {
        let hasPermission = false;
        for (let j = 0, func; func = functions[j++];) {
            if (func.code === item.name && (allowedFunctions[func.code])) {
                item.uri = func.uri || item.uri || 'javascript:;';
                item.icon = func.icon_url || item.icon;
                item.target = item.target || '_self';
                item.children = item.children || [];
                item.opened = false;
                hasPermission = true;
                break;
            }
            if (allowedFunctions['all']) {
                hasPermission = true;
            }
        }
        item.show = hasPermission === true;

        travelMenu(item.children, functions, allowedFunctions);
    }
}

function walkMenu(menu, key, process, level = 1) {
    let finded = false;
    for (let i = 0; i < menu.length; i++) {
        const item = menu[i];
        process(item);
        if (item.key === key) {
            finded = true;
            break;
        }
        if (item.children && walkMenu(item.children, key, process, level + 1)) {
            finded = true;
            break;
        }
        process('', true);
    };
    return finded;
}

function getKeyPath(key) {
    return menuPromise.then((menu) => {
        const keyPath = [];

        walkMenu(menu.toJSON ? menu.toJSON() : menu, key, function (item, shift) {
            if (shift) {
                keyPath.shift();
            } else {
                keyPath.unshift(item);
            }
        });

        return keyPath;
    });
}
export {
    getKeyPath,
    menuPromise as menu,
    menubar,
    sysMenu
};