/**
 * ======================系统常规配置======================
 * 系统全局标题、版权、单点登录、默认浏览器下载等配置项
 */

module.exports.titleName = 'XX省公安厅执法视音频管理平台';

module.exports.copyRight = 'Copyright © 2005-2018，高新兴国迈科技有限公司，版权所有';

module.exports.telephone = '技术支持：TEL 400 6578 900';

module.exports.defalutCity = "广州";

// singleSignOn 单点登录标志 false => 手动登录， true => 自动登录
module.exports.singleSignOn = false;

// 单点登录gt参数
module.exports.gt = '74df37c22e352ea2e2fc1a885dcd825e';

// 高新兴国迈安全浏览器默认下载  0 => 火狐浏览器（定制版）   1 => 谷歌浏览器（定制版）  默认为0
module.exports.defaultBrowser = 0;

module.exports.accountType = "permanent|temporary"; //账号类型： permanent 永久用户，temporary 临时用户

// 平台版本
module.exports.platformVersion = 1; // 0 => 公安版    1 => 交警版

module.exports.pageSize = 10;

/**
 * 首页自定义版本
 * @param mainIndex  main_index 主线版本  main_sdjj 山东 版本
 * 
 */

module.exports.mainIndex = 'main_index';

/* 
 *  =============================实时指挥系统相关配置项 =============================
 * 
 *  地图配置说明，针对打包后的文件夹，在sszhEsriMap.html改.css,.js文件ip和端口，在window.mapConfig里面改地图瓦片数据server ip和端口
 *
 *  地图配置，0是高新兴地图，1是百度在线地图 2是mapLite（深圳项目）
 *  配置为mapLite时，可在mapLite.html中配置不同的底图
 *  配置代码位置：
 *  map = new MapLite.Map("mapDiv", {
        center: tmpPoint,
        level: 12,
        isInertiaDrag: false,
        basemap: "online/tdt/map"//可以配置不同底图："online/baidu/map", "online/google/map" , "online/qq/map", "online/amap/map", "online/tdt/map"
                                //分别为"百度", "谷歌", "腾讯", "高德", "天地图"
    });
*
* */

module.exports.mapType = 0;

module.exports.webSocketIp = (window.location.host == '127.0.0.1:3000' || window.location.host == '10.10.16.169:3000') ? '10.10.9.101:8201' : window.location.host;

//实时指挥是否允许语音
module.exports.isAllowSpeak = true;

//实时指挥是否允许视频
module.exports.isAllowVideo = true;

//实时指挥是否允许录像
module.exports.isAllowRecord = false;

//实时指挥是否允许拍照
module.exports.isAllowPhotograph = false;

//实时指挥是否允许锁定
module.exports.isAllowLock = false;


/**
 * ======================OCX播放器相关配置======================
 * 
 */

//视音频播放器配置
module.exports.playerConfig = true; //false h5;true webplayer

//高新兴ocx图像增强功能输出文件夹
module.exports.imageEnhanceOutPut = "C:\\Windows\\Temp\\imageEnhanceOutPut\\";

//高新兴ocx视频播放截图输出文件夹
module.exports.screenShotOutPut = "D:\\screenShotOutPut";

//高新兴ocx现使用的版本号
module.exports.gxxOcxVersion = "4.0.0.40";


/**
 * ======================系统固定常量配置======================
 * 开发、打包环境变量，勿修改
 */

module.exports.domain = '__DOMAIN__';

module.exports.serviceUrl = '__SERVICE_URL__';

module.exports.apiUrl = '__API_URL__';